import React, { Component } from "react";
import "./css/style.css"
import "./css/phone-ring.css"


import "./App.css";
import Dashboard from "./components/Dashboard";
import Header from "./components/Layout/Header";
// import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AddTask from "./components/Task/AddTask";
import { Provider } from "react-redux";
import store from "./store";
import UpdateTask from "./components/Task/UpdateTask";
import TaskBoard from "./components/ProjectBoard/TaskBoard";
import AddIncidentTask from "./components/ProjectBoard/IncidentTasks/AddIncidentTask";
import UpdateIncidentTask from "./components/ProjectBoard/IncidentTasks/UpdateIncidentTask";
import Landing from "./components/Layout/Landing";
import Register from "./components/UserManagement/Register";
import Login from "./components/UserManagement/Login";
import jwt_decode from "jwt-decode";
import setJWTToken from "./securityUtils/setJWTToken";
import { SET_CURRENT_USER } from "./actions/types";
import { logout } from "./actions/securityActions";
import SecuredRoute from "./securityUtils/SecureRoute";

import {containerService, INSTRUCTIONS, REDIRECT} from "./container_utils/index";

import {auth} from "./auth/index";

// import "./plugins/common/common.min.js"
// import "./js/custom.min.js"
import "./js/settings.js"

// import "./js/gleek.js"
import "./js/styleSwitcher.js"


// const jwtToken = localStorage.jwtToken;

// if (jwtToken) {
//   setJWTToken(jwtToken);
//   const decoded_jwtToken = jwt_decode(jwtToken);
//   store.dispatch({
//     type: SET_CURRENT_USER,
//     payload: decoded_jwtToken
//   });

//   const currentTime = Date.now() / 1000;
//   if (decoded_jwtToken.exp < currentTime) {
//     store.dispatch(logout());
//     window.location.href = "/";
//   }
// }

class App extends Component {

  constructor(props){
    super(props);
    this.state={
      tokenFetched:true    
    }         
  }

  async fetchData(){
    
    console.log("fetching token");
    let container = containerService.getInstance();
    // first get the token
    let userInfo = await container.synchronizedSend({type: INSTRUCTIONS.AUTH_GETTER})
    console.log("%c inside the admin-frontend", "font-size:50px; color:pink")
    // if valid or redirect to the login page
    console.log(userInfo)
    // console.log(userInfo.user.roleList) 
    // console.log(userInfo.value.user.id)
    if(userInfo===undefined || userInfo ===null)
        container.send({type: INSTRUCTIONS.REDIRECT, value:REDIRECT.LOGIN})
    else{
      await auth.getInstance()
          .setToken(userInfo.value.token)
      console.log("after setting token")
      console.log(auth.getInstance().getToken())
      this.setState({
          tokenFetched:true
      })
    }  
  }
  
  componentDidMount(){
    console.log("cc component mounted");
    this.fetchData();
    // this.queueListener();
  }
    // You can await here

  render() {

  return (
      <Provider store={store}>
        <Router>
          <div className="App">
          {
          this.state.tokenFetched===true?
          <div className="col-md-12 col-sm-12">
          <Header />
          <Switch>
          <Route exact path="/" component={Dashboard} />
            <Route exact path="/addIncident" component={AddTask} />
            <Route
              exact
              path="/updateIncident/:id"
              component={UpdateTask}
            />
            <Route
              exact
              path="/myTasksBoard/:id"
              component={TaskBoard}
            />
            <Route
              exact
              path="/addTask/:id"
              component={AddIncidentTask}
            />
            <Route
              exact
              path="/updateTask/:backlog_id/:pt_id"
              component={UpdateIncidentTask}
            />
          </Switch>
          </div>:
          <div className={"page-center loader"}/>
          }
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
