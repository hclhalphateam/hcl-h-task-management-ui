import axios from "axios";
import {
  GET_ERRORS,
  GET_BACKLOG,
  GET_INCIDENT_TASK,
  DELETE_INCIDENT_TASK
} from "./types";

//Fix bug with priority in Spring Boot Server, needs to check null first
export const addIncidentTask = (
  backlog_id,
  project_task,
  history
) => async dispatch => {
  try {
    await axios.post(`https://localhost:8080/api/backlog/${backlog_id}`, project_task);
    history.push(`/myTasksBoard/${backlog_id}`);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};

export const getBacklog = backlog_id => async dispatch => {
  try {
    const res = await axios.get(`https://localhost:8080/api/backlog/${backlog_id}`);
    dispatch({
      type: GET_BACKLOG,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};

export const getIncidentTask = (
  backlog_id,
  pt_id,
  history
) => async dispatch => {
  try {
    const res = await axios.get(`https://localhost:8080/api/backlog/${backlog_id}/${pt_id}`);
    dispatch({
      type: GET_INCIDENT_TASK,
      payload: res.data
    });
  } catch (err) {
    history.push("/");
  }
};

export const updateIncidentTask = (
  backlog_id,
  pt_id,
  project_task,
  history
) => async dispatch => {
  try {
    await axios.patch(`https://localhost:8080/api/backlog/${backlog_id}/${pt_id}`, project_task);
    history.push(`/myTasksBoard/${backlog_id}`);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};

export const deleteIncidentTask = (backlog_id, pt_id) => async dispatch => {
  if (
    window.confirm(
      `You are deleting this task ${pt_id}, this action cannot be undone`
    )
  ) {
    await axios.delete(`https://localhost:8080/api/backlog/${backlog_id}/${pt_id}`);
    dispatch({
      type: DELETE_INCIDENT_TASK,
      payload: pt_id
    });
  }
};
