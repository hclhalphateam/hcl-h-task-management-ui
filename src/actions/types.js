export const GET_ERRORS = "GET_ERRORS";
export const GET_TASKS = "GET_TASKS";
export const GET_TASK = "GET_TASK";
export const DELETE_TASK = "DELETE_TASK";

//Types for BACKLOG ACTIONS

export const GET_BACKLOG = "GET_BACKLOG";
export const GET_INCIDENT_TASK = "GET_INCIDENT_TASK";
export const DELETE_INCIDENT_TASK = "DELETE_INCIDENT_TASK";

export const SET_CURRENT_USER = "SET_CURRENT_USER";
