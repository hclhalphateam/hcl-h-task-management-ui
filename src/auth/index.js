
export const auth = (function(){
    let token=null;
    function init() {
        return {
            getToken: ()=>token,
            setToken: function (tokenVal){
                token = tokenVal
            }
        };
    }
    return {
        getInstance: ()=>init()
    }
})();