import React,{Component} from 'react';
import axios from 'axios';
import { ScheduleComponent, Day, Week,Month, ViewsDirective, ViewDirective,Inject } from '@syncfusion/ej2-react-schedule';


const API_PATH = 'https://exit-dsp.hclhealthcare.in:5001/api';
class Calendar extends Component {
    constructor() {    	

        super(...arguments);

        this.state = {
    		json_data:[
    		{
    			data_type: "provider_availability",
    			data_value:[1],
    			optional_search_filters:[
    			{
    				offset: "",
    				type  : "",
    				value : []
    			}
    			]
    		}
    		],    		
    		api_data          : ''
    	}

    	this.fetchProvider = () => {		
		// console.log(this.state);
		axios({
			method: 'POST',
			url: `${API_PATH}`,
			headers: {
				'content-type': 'application/json',
			},
			data: this.state.json_data[0],
		})
		.then(response => {      
			// console.log(response.data);	
			this.setState({api_data:response.data});			
		})
		.catch(error => {
			console.log(error);			
			throw error;
		})		
	} 
	
	this.showCalendar = (e) =>{		

			var check_provider_id  = e.target.value;
			var show_status        = e.target.checked;				
	}

	// 	this.data = [
	// 	{
	// 		Id: 2,
	// 		Subject: 'booked',
	// 		StartTime: new Date(2020, 6, 15, 10, 0),
	// 		EndTime: new Date(2020, 6, 15, 10, 30),
	// 	},
	// 	{
	// 		Id: 3,
	// 		Subject: 'booked',
	// 		StartTime: new Date(2020, 6, 15, 11, 0),
	// 		EndTime: new Date(2020, 6, 15, 11, 30),
	// 	}
	// 	];


    }

    render() {
        return (
        	<>
        	{/* <input type="button" onClick={this.fetchProvider} className="form-control input-default" data-toggle="modal" data-target="#myDoctor"  value="Check Doctor Availability" placeholder="Doctor List" /> */}
	
		<input type="text" className="form-control input-default" placeholder="Doctor's List" onClick={this.fetchProvider}  data-toggle="modal" data-target="#myDoctor"/>
		
		{/* <button type="button"   value="Check Doctor Availability" placeholder="Doctor List" className="btn btn-primary">Check Doctor Availability</button> */}
			
			<div className="modal fade" id="myDoctor" role="dialog">
			<div className="modal-dialog">		  
			<div className="modal-content">
			<div className="modal-header">
			<h4>Book Your Doctor</h4>
			<button type="button" className="close" data-dismiss="modal">X</button>
			</div>
			<div className="modal-body">
			<div className="row">
			<div className="col-md-2">		  
			<h4>Doctor's List</h4>
			<ul className="doctorlist">
			{				
				this.state.api_data!== '' ?
				this.state.api_data.data?		
				this.state.api_data.data.map(response => {
					return(
						<li key={response.provider_id}>
							<span className="doctorname">
							<input type="checkbox" onClick={e => this.showCalendar(e)} value={response.provider_id}/>
							<b>{response.provider_name}</b>
							</span>
						</li>
						)
					})
					:this.state.api_data.message
					:""
				}
				</ul>

				</div>
				<div className="col-md-10" id="myid">
					<div className="row">
					{				
					this.state.api_data!== '' ?
					this.state.api_data.data ?		
					this.state.api_data.data.map(response => {	
						this.newdata = [];
						response.slots.map(slot_data => {																
							this.newdata.push(
							{								
								Id: response.id,
								Subject: slot_data.slot_status,
								StartTime: new Date(response.availability_date+'T'+slot_data.slot_start_time),
								EndTime: new Date(response.availability_date+'T'+slot_data.slot_end_time)
							})								
						})

						console.log(this.newdata);
						return(
							<div key={response.provider_id} id={response.provider_id}>	
							<div className="calendarbox">					
							<h4>{response.provider_name}</h4>

							<ScheduleComponent 						
							height='400px' 
							selectedDate={new Date()} 
							eventSettings={{dataSource: this.newdata }}>

							<ViewsDirective>
							<ViewDirective option='Day'/>
							{/* <ViewDirective option='Week'/>					 */}
							<ViewDirective option='Month'/>
							</ViewsDirective>
							<Inject services={[Day, Week, Month]}
							/>
							</ScheduleComponent>
							</div>
							</div>
							)					
						})
						:""
						:""
					}
					</div>
				</div>
				</div>

				<div className="modal fade none-border" id="event-modal">
				<div className="modal-dialog">
				<div className="modal-content">
				<div className="modal-header">
				<h4 className="modal-title"><strong>Add New Event</strong></h4>
				</div>
				<div className="modal-body"></div>
				<div className="modal-footer">
				<button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" className="btn btn-success save-event waves-effect waves-light">Create event</button>
				<button type="button" className="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
				</div>
				</div>
				</div>
				</div>
				</div>
				<div className="modal-footer">
				<button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>                                       
				</div>
				</div>

        	</>
    );
    }
}
;
export default Calendar;