import React, { Component } from "react";
import TaskItem from "./Task/TaskItem";

import TaskPane from "./Task/TaskPane";
import Ameyo from "../view/Ameyo"
import CreateTaskButton from "./Task/CreateTaskButton";
import { connect } from "react-redux";
import { getTasks , getAllTasks} from "../actions/taskActions";

import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import ClickToCall from "./Task/ClickToCall";
import "../css/call/index.css";
import axios from 'axios';

// import '../js/agent-role'
import "../css/call/callincoming.css";

import {containerService, INSTRUCTIONS, REDIRECT} from "../container_utils/index";
import Calendar from "./Calendar1";
import CallIncoming from "./Task/CallIncoming";

// import Pagination from "react-js-pagination";
import {filterByValue, loadExactPage, loadNewPage, sortByAlphabet, sortByPrice} from "../actions/taskActions";

// import FilterTasks from "./Task/FilterTasks";
import Pagination from './Task/Pagination';
import {Spinner} from 'react-bootstrap';
class Dashboard extends Component {

  constructor(){
    super();
    this.state = {
      data: "",
      taskData:[],
      SMSData:[],
      tasknote:"",
      tasknotes:[],
      pageLimit : 10,
      activePage: 1,
      incomingphone:"",
      phoneincoming: false,
      filterOption: '',
      filteredData: [],
      totalcount:0,
      showResults : false,
      searchTerm:'',
      pageOfItems: [],
      serviceData:[],
      tasknotes:[],
      packageId:0,
      callWrapperCollapse:false,
      fetchInProgress: true
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.sortByInput = this.sortByInput.bind(this);
    this.sortAsc = this.sortAsc.bind(this);
    this.sortDesc = this.sortDesc.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
  }

  onChangePage(pageOfItems) {
      // update state with new page of items    
      this.setState({ pageOfItems: pageOfItems });
  }
  optionSelected(e) {
    this.setState({
      filterOption: e.target.value
    })
    if( e.target.value === 'projectIdentifier' || e.target.value === 'employeeId' || e.target.value === 'projectName'){
      this.setState({showResults:true});
      console.log(this.state.showResults);
      console.log(e.target.value);
    }
    else{
      this.setState({showResults:false});
      console.log(this.state.showResults);
    }
  }

  sortAsc(arr, field) {
    console.log(arr);
    console.log(field);
    return arr.sort(function(a, b) {
      if (a[field].toLowerCase() > b[field].toLowerCase()) return 1;
      if (b[field].toLowerCase() > a[field].toLowerCase()) return -1;
      return 0;
    });
  }

  sortDesc(arr, field){
    console.log(arr);
    console.log(field);
    return arr.sort(function(a, b) {
      if (a[field].toLowerCase() > b[field].toLowerCase()) return -1;
      if (b[field].toLowerCase() > a[field].toLowerCase()) return 1;
      return 0;
    });
  }

  renderTableData(data) {
    console.log(data);
    return  data.map(project => (
      <TaskItem key={project.id} project={project} 
      changeLink={this.onChangeLinkName.bind(this)}
      />
    ))
  }

  renderFilterOptions(filterOption,data) {
    if(filterOption) {
      console.log(filterOption);
      let options = Array.from(new Set(data.map(element => element[filterOption])));
      return options.map((option,i) => <option key={i} value={option}> {option} </option>)
    }  
    return []
  }

  filterOptionSelected(e) {
    let {value} = e.target
    console.log(value);
      let { filterOption } = this.state
      if(filterOption === 'projectIdentifier' || filterOption === 'employeeId' || filterOption === 'projectName'){
      
      const { projects } = this.props.project;
      let filteredData = projects.filter(element => element[filterOption].toLowerCase().includes(value.toLowerCase()));
    console.log(filteredData);
      this.setState({
        filteredData: filteredData
      })
    }

    else{
      if(value) {
        let { filterOption } = this.state
        const { projects } = this.props.project;

        let filteredData = projects.filter(element => element[filterOption] == value);
        this.setState({
          filteredData: filteredData
        })
      }
    }
  }

  filterByInput(e){
    let input = e.target.value;
    this.props.filterByValue({value: input})
  }
  nextPage() {
    this.props.dispatch(loadNewPage({page: 1}))
  }
  previousPage() {
    this.props.dispatch(loadNewPage({page: -1}));
  }
  goToPage(page) {
    this.props.dispatch(loadExactPage({page}))
  }
  sortByInput(e){
    let value = e.target.value;
    console.log(value);
    const { projects } = this.props.project;

    if(value.endsWith('none')){
    this.setState({
      filteredData: this.state.pageOfItems
    })
  }
  else{

    let direction = value.endsWith('asc') ? "asc" : "desc";

    let sortedAlphabetArr =
    direction === "asc"
      ? this.sortAsc(this.state.pageOfItems,"projectName")
      : this.sortDesc(this.state.pageOfItems,"projectName");

      console.log(sortedAlphabetArr);
      this.setState({
      // filteredData: sortedAlphabetArr,
      pageOfItems: sortedAlphabetArr
    })
  }
    // if (value.startsWith('price')){
    //     this.props.sortByPrice({direction})
    // }else {
    //     this.props.sortByAlphabet({direction});
    // }
  }

  togglephoneincoming(val){
    this.setState({phoneincoming:val});
  }
  
  emitincomingphone(val){
    this.setState({incomingphone:val});
  }
     
  handlePageChange(pageNumber) {
    // console.log(`active page is ${pageNumber}`);
    
    console.log(this.props.getTasks(pageNumber - 1,this.state.pageLimit));
    console.log("%c gET tASKS event:: : ", "font-size:48px; color:black");
    
    this.setState({activePage: pageNumber});
  }

  toggleSideNav(val){
    this.setState({
      callWrapperCollapse:val
    })
}

  queueListener(){

    let containerInstance = containerService.getInstance()
    setInterval(()=>{
        if(containerInstance.queue.length!==0){
            switch(containerInstance.queue[0].type){
                case INSTRUCTIONS.HEADER_COLLAPSE:
                    console.log("header-collapse received in the app")
                    console.log("value: " +  containerInstance.queue[0].value)
                    //do something here
                    this.toggleSideNav(containerInstance.queue[0].value)
                    containerInstance.queue.splice(0, 1);
                    console.log("queue curr stats")
                    console.log("value: " +  containerInstance.queue)
            }
        }
    }, 60)
  }
  componentDidMount() { 
    console.log(this.props);  
    // this.handlePageChange(this.state.activePage);
    // this.props.getTasks(0,10);

    this.props.getAllTasks();
    // console.log(alltaskData);
    // axios.get('https://localhost:8080/api/gettotaltaskscount')
    //   .then(res => {
    //     const totalcount = res.data;
    //     console.log(res);
    //     this.setState({    totalcount });
    //   })
      this.queueListener();
  }

  onChangeState(data){
    console.log("change state to false")
    // console.log(data)
    this.setState({phoneincoming:data});
    console.log(this.state.phoneincoming);
    
    function handleHangup(reason, userCustomerCRTInfo) {
              
      console.log("%c Ameyo event:: call dispose: ", "font-size:48px; color:orange");
      console.log(reason);	
      console.log(userCustomerCRTInfo);	
    }
    let customIntegration = {};
    customIntegration.hangupHandler = handleHangup;
    window.ameyo.integration.registerCustomFunction("hangupHandler", customIntegration);
  }

  onChangeLinkName(data){

    // console.log(this.props.data.patientMobile);
    axios.get('https://scheduling-dsp.hclhealthcare.in:8086/v1/getSMSByPhone?no='+data.patientMobile
    )
    .then(res => {
      const SMSData = res.data;
      this.setState({    SMSData });
    });

    let services= JSON.parse(data.services);

    this.setState({serviceData:services});

    axios.get('https://task-dsp.hclhealthcare.in:8080/v1/tasknotebypan?pan='+data.projectIdentifier
    )
    .then(res => {
      const tasknotes = res.data;
      this.setState({    tasknotes });
    });    
        if(data.description.includes("Diabetes Care Plan")){
          this.setState({packageId:1});
      }
      else if(data.description.includes("Pre Diabetes Care Plan")){
        this.setState({packageId:2});
      }
      else if(data.description.includes("Weight Management Care Plan")){
        this.setState({packageId:5});
      }
      else if(data.description.includes("Healthy Heart Care Plan")){
        this.setState({packageId:4});
      }
      else if(data.description.includes("Cardiovascular Care Plan")){
        this.setState({packageId:3});
      }
      else{
        this.setState({packageId:0});
      }
    this.setState({
        data: data
    });
} 
  render() {
    // let products = this.props.state.filteredProducts;

    let { filterOption, filteredData } = this.state
    const { projects } = this.props.project;
    // console.log(projects);
    // this.setState({taskData:projects});

    const collpaseSideCall = <div className={'collapsed-side-call'}><i class="icon-phone menu-icon call-btn"></i></div>
    const MyTaskClass = this.state.callWrapperCollapse===false?
    'col-md-5 col-sm-12'
    :'col-md-5 col-sm-12';

    const TaskPaneClass = this.state.callWrapperCollapse===false?
    'col-md-5 col-sm-12'
    :'col-md-7 col-sm-12';

    return (
      // <div className="projects">

        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-sm-12">
      {
        (this.state.phoneincoming=== true) ?
        <CallIncoming data={this.state.incomingphone}   changeState={this.onChangeState.bind(this)}></CallIncoming>
        :
        ''
      }
      <Ameyo></Ameyo>     
      </div>
          {/* <div className="container">
          <div className="row"> */}
          {/* { 
          this.state.callWrapperCollapse===false?
          <ameyo/>
          :
          collpaseSideCall
          } */}

          <div className={MyTaskClass}>
              {/* <h1 className="display-4 text-center">My Tasks</h1> */}
              {/* <br /> */}
              {/* <CreateProjectButton /> */}
              {/* <br />
              <hr /> */}

            <div className="">
            <ul className="nav nav-tabs mb-3">
              <li className="nav-item">
                <a className="nav-link active" data-toggle="tab" href="#home1">
                  My Tasks
                </a>
              </li>
              <li className="nav-item">
              
              <a className="nav-link" href="/addIncident">
                        
                <i className="fas fa-plus-circle"> </i> Create a Task
                </a>
                </li>
            </ul>
            <div className="card">
              <div className="custom-tab-1">
                <div className="tab-content">
                  <div className="tab-pane fade show active" id="home1" role="tabpanel">
                    <div
                      className="col-md-12"
                      style={{ borderBottom: "1px solid #33333314" }}
                    >
                      <form>
                        <div className="advancesearch1" style={{ display: "none" }}>
                          <div className="form-row">
                            <div className="form-group col-md-9">
                              <input
                                type="text"
                                className="form-control input-default"
                                placeholder="Search"
                              />
                              <a href="#" className="advancesearch">
                                Advanced Search
                              </a>
                            </div>
                            <div className="form-group col-md-3">
                              <button
                                type="submit"
                                className="btn btn-primary form-control input-default"
                              >
                                Search
                              </button>
                            </div>
                          </div>
                        </div>
                        <div>
                            {/* <div className="field is-grouped" style={{alignItems: "center"}}>
                                <div className="control">
                                    <div className="select">
                                        <select onChange={e => {
                                            this.sortByInput(e)
                                        }}>
                                            <option value="" disabled selected>Sort by</option>

                                            <option value='alphabet_asc'>Name - A-Z</option>
                                            <option value='alphabet_desc'>Name - Z-A</option>

                                            <option value='price_asc'>Price - Lowest to Highest</option>
                                            <option value='price_desc'>Price - Highest to Lowest</option>

                                        </select>
                                    </div>
                                </div>

                                <div className='control' style={{minWidth: "300px"}}>
                                    <input onChange={e => {
                                        this.filterByInput(e);
                                    }} style={{width: "100%"}} placeholder='Filter by' type='text'/>
                                </div>
                            </div> */}
                        </div>
              <div className="filtersearch" style={{ display: "block" }}>
                <a href="#" className="normalsearch">
                  Normal Search
                </a>
                <div className="form-row">
                <div className="form-group col-md-4">
                    {/* <button
                      type="submit"
                      className="btn btn-primary form-control input-default"
                    >
                      Search
                    </button> */}
                     <select className="form-control input-default" onChange={
                                            this.sortByInput}>
                  
                      <option value=""  disabled selected>Sort By</option>
                  
                      <option value='alphabet_asc'>Name - A-Z</option>
                      <option value='alphabet_desc'>Name - Z-A</option>
                      {/* <option value='alphabet_none'>None</option> */}

                  </select>
                  </div>
                  <div className="form-group col-md-4">
                    <select className="form-control input-default"  onChange={this.optionSelected.bind(this)}>
                      <option value="">  Filter By</option>
                    <option value="employer"> Employer </option>
                    <option  value="facilityName"> Facility </option>
                    <option  value="description"> Care Plan</option>
                    <option  value="projectName">Patient Name</option>
                    <option  value="projectIdentifier">PAN</option>
                    <option  value="employeeId">Employee Id</option>

                    {/* <option  value="e"> Care Plan</option> */}
                    
                    
                  
                      {/* <option>HCL HealthCare Gurgaon Center</option>
                      <option>HCL HealthCare Bangalore Sez Center</option>
                      <option>
                        HCL HealthCare Bangalore Sez Corporate Center
                      </option>
                      <option>HCL HealthCare Noida Sez Center</option>
                      <option>HCL HealthCare Noida Sez Corporate Center</option> */}
                    </select>
                  </div>
                  {/* <div className="form-group col-md-3">
                    <select className="form-control input-default">
                      <option>Type</option>
                      <option>HCLT Employee</option>
                      <option>HCLT Dependent</option>
                      <option>HCL HealthCare Dependent</option>
                      <option>HCL Foundation</option>
                      <option>Shiv Nadar Foundation SNF</option>
                      <option>HCL Corp</option>
                      <option>HCL Corp Dependent</option>
                    </select>
                  </div> */}
                  <div className="form-group col-md-4">

                  { this.state.showResults === false ?
                    <select className="form-control input-default" onChange={this.filterOptionSelected.bind(this)}>
                    <option value=""> Values </option>
                    {this.renderFilterOptions(filterOption,projects)}
                    
                    </select>
                    :
                    <input type="text" className="form-control input-default" placeholder="Text to Search"
                     name="textsearch" onChange={this.filterOptionSelected.bind(this)}
                     
                     />
                }
                  </div>

              
                </div>
              </div>
            </form>
          </div>
          <div className="col-md-12">
            <div id="message-panel">
              <ul id="messages">
              {/* {projects.map(project => (
                <TaskItem key={project.id} project={project} 
                changeLink={this.onChangeLinkName.bind(this)}
                />
              ))} */}
               {
              //  filteredData.length? this.renderTableData(filteredData) : this.renderTableData(this.state.pageOfItems) 
              this.state.pageOfItems.length?
              this.renderTableData(this.state.pageOfItems):
              // <div class="message-panel">
              //   <div class="card"
              // >
                 <img className="imgsize" src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" />
                
              //   </div>
              // </div>
             }
              </ul>
            </div>            
          </div>        
        </div>
  
        <div className="tab-pane fade" id="mytask">
          <div className="p-t-15">
            <h4>2This is profile title</h4>
            <p>
              Raw denim you probably haven't heard of them jean shorts Austin.
              Nesciunt tofu stumptown aliqua, retro synth master cleanse.
              Mustache cliche tempor.
            </p>
            <p>
              Raw denim you probably haven't heard of them jean shorts Austin.
              Nesciunt tofu stumptown aliqua, retro synth master cleanse.
              Mustache cliche tempor.
            </p>
          </div>
        </div>        
      </div>
    </div>

    {/* <Pagination
        activePage={this.state.activePage}
        itemsCountPerPage={this.state.pageLimit}
        totalItemsCount={this.state.totalcount}
        pageRangeDisplayed={5}
        onChange={this.handlePageChange}
    /> */}

      <Pagination 
      items = {filteredData.length? filteredData : projects }  
      // items={projects} 
      onChangePage={this.onChangePage} />
  </div>
</div></div>
              {/* <Calendar/> */}
            <div className={TaskPaneClass}>
              <TaskPane smsdata={this.state.SMSData} notesdata={this.state.tasknotes} packageid={this.state.packageId} servicedata={this.state.serviceData}  data={this.state.data} togglephoneincoming={(val)=>this.togglephoneincoming(val)}
              emitincomingphone={(val)=> this.emitincomingphone(val)} />
            </div>
           {/* <Calendar></Calendar> */}
          </div>
        </div>
      // </div>
    );
  }
}

Dashboard.propTypes = {
  project: PropTypes.object.isRequired,
  getTasks: PropTypes.func.isRequired,
  getAllTasks: PropTypes.func.isRequired,
  filterByValue: PropTypes.func.isRequired,
   loadExactPage:PropTypes.func.isRequired,
    loadNewPage: PropTypes.func.isRequired,
    sortByAlphabet: PropTypes.func.isRequired,
    sortByPrice: PropTypes.func.isRequired
  
};

const mapStateToProps = state => ({
  project: state.project
});

export default connect(
  mapStateToProps,
  { getTasks , getAllTasks,  filterByValue,
    loadExactPage,
     loadNewPage,
     sortByAlphabet,
     sortByPrice}
)(Dashboard);