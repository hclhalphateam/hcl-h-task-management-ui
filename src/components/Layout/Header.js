import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logout } from "../../actions/securityActions";

class Header extends Component {
  logout() {
    this.props.logout();
    window.location.href = "/";
  }
  render() {
    
    // const { validToken, user } = this.props.security;

    // const userIsAuthenticated = (
    //   <div className="collapse navbar-collapse" id="mobile-nav">
    //     <ul className="navbar-nav mr-auto">
    //       <li className="nav-item">
    //         <Link className="nav-link" to="/">
    //           Dashboard
    //         </Link>
    //       </li>
    //     </ul>

    //     <ul className="navbar-nav ml-auto">
    //       <li className="nav-item">
    //         <Link className="nav-link" to="/">
    //           <i className="fas fa-user-circle mr-1" />
    //           {user.fullName}
    //         </Link>
    //       </li>
    //       <li className="nav-item">
    //         <Link
    //           className="nav-link"
    //           to="/logout"
    //           onClick="{this.logout.bind(this)}"
    //         >
    //           Logout
    //         </Link>
    //       </li>
    //     </ul>
    //   </div>
    // );

    // const userIsNotAuthenticated = (
    //   <div className="collapse navbar-collapse" id="mobile-nav">
    //     <ul className="navbar-nav ml-auto">
    //       <li className="nav-item">
    //         <Link className="nav-link" to="/register">
    //           Sign Up
    //         </Link>
    //       </li>
    //       <li className="nav-item">
    //         <Link className="nav-link" to="/login">
    //           Login
    //         </Link>
    //       </li>
    //     </ul>
    //   </div>
    // );

    let headerLinks;

    // if (validToken && user) {
      // headerLinks = userIsAuthenticated;
    // }
    //  else {
    //   headerLinks = userIsNotAuthenticated;
    // }

    return (
      // <nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-4">
      //   <div className="container">
      //     <Link className="navbar-brand" to="/">
      //       <b>HCL Healthcare</b>
      //     </Link>
      //     <button
      //       className="navbar-toggler"
      //       type="button"
      //       data-toggle="collapse"
      //       data-target="#mobile-nav"
      //     >
      //       <span className="navbar-toggler-icon" />
      //     </button>
      //     {headerLinks}
      //   </div>
      // </nav>
<div>
      {/* <div className="nav-header">
  <div className="brand-logo">
    <a href="/">
      <b className="logo-abbr">HCL</b>
      <span className="logo-compact">HCL</span>
      <span className="brand-title">HCL HealthCare</span>
    </a>
  </div>
</div> */}
      {/* <div className="header">
      <div className="header-content clearfix">
        <div className="header-right">
          <ul className="clearfix">
            <li className="icons dropdown">
              <a
                href="javascript:void(0)"
                data-toggle="dropdown"
                aria-expanded="false"
              >
                <i className="mdi mdi-bell-outline" />
                <span className="badge badge-pill gradient-2">3</span>
              </a>
              <div
                className="drop-down animated fadeIn dropdown-menu dropdown-notfication"
                x-placement="bottom-start"
                style={{
                  position: "absolute",
                  willChange: "transform",
                  top: 0,
                  left: 0,
                  transform: "translate3d(5px, 49px, 0px)"
                }}
              >
                <div className="dropdown-content-heading d-flex justify-content-between">
                  <span className>2 New Notifications</span>
                  <a href="javascript:void()" className="d-inline-block">
                    <span className="badge badge-pill gradient-2">5</span>
                  </a>
                </div>
                <div className="dropdown-content-body">
                  <ul>
                    <li>
                      <a href="javascript:void()">
                        <span className="mr-3 avatar-icon bg-success-lighten-2">
                          <i className="icon-present" />
                        </span>
                        <div className="notification-content">
                          <h6 className="notification-heading">Call Back Later</h6>
                          <span className="notification-text">
                            Within next 5 mins
                          </span>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:void()">
                        <span className="mr-3 avatar-icon bg-danger-lighten-2">
                          <i className="icon-present" />
                        </span>
                        <div className="notification-content">
                          <h6 className="notification-heading">
                            Call Not Connected
                          </h6>
                          <span className="notification-text">One hour ago</span>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:void()">
                        <span className="mr-3 avatar-icon bg-success-lighten-2">
                          <i className="icon-present" />
                        </span>
                        <div className="notification-content">
                          <h6 className="notification-heading">
                            Send Invite to Deepak
                          </h6>
                          <span className="notification-text">One hour ago</span>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:void()">
                        <span className="mr-3 avatar-icon bg-danger-lighten-2">
                          <i className="icon-present" />
                        </span>
                        <div className="notification-content">
                          <h6 className="notification-heading">Reply to Pankaj</h6>
                          <span className="notification-text">After two days</span>
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li className="icons dropdown d-none d-md-flex">
              <a
                href="javascript:void(0)"
                className="log-user"
                data-toggle="dropdown"
                aria-expanded="false"
              >
                <span>Agent</span>{" "}
                <i className="fa fa-angle-down f-s-14" aria-hidden="true" />
              </a>
              <div
                className="drop-down dropdown-language animated fadeIn dropdown-menu"
                x-placement="bottom-start"
                style={{
                  position: "absolute",
                  willChange: "transform",
                  top: 0,
                  left: 0,
                  transform: "translate3d(5px, 43px, 0px)"
                }}
              >
                <div className="dropdown-content-body">
                  <ul>
                    <li>
                      <a href="javascript:void()">Supervisor</a>
                    </li>
                    <li>
                      <a href="javascript:void()">Admin</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li className="icons dropdown">
              <div
                className="user-img c-pointer position-relative"
                data-toggle="dropdown"
              >
                <span className="activity active" />
                <img src="../../images/user/1.png" height={40} width={40} alt />
              </div>
              <div className="drop-down dropdown-profile animated fadeIn dropdown-menu">
                <div className="dropdown-content-body">
                  <ul>
                    <li>
                      <a href="#">
                        <i className="icon-user" /> <span>Profile</span>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:void()">
                        <i className="icon-envelope-open" /> <span>Inbox</span>
                        <div className="badge gradient-3 badge-pill gradient-1">
                          3
                        </div>
                      </a>
                    </li>
                    <hr className="my-2" />
                    <li>
                      <a href="page-lock.html">
                        <i className="icon-lock" /> <span>Lock Screen</span>
                      </a>
                    </li>
                    <li>
                      <a href="page-login.html">
                        <i className="icon-key" /> <span>Logout</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>       
    <br/> */}
    <br/>
</div>
      );

  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  security: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  security: state.security
});

export default connect(
  mapStateToProps,
  { logout }
)(Header);
