import React from "react";
import {Button, Modal, Nav} from 'react-bootstrap';

class Calendar extends React.Component{

    constructor() {
        super();
        this.state = {
            users:null,
            show:false, 
            user:{}
        }
    }

    
    handleModal(){
        this.setState({ show: !this.state.show })
    }

    render() {
        return (
            <div className="container">
                <input type="type" className="form-control input-default" data-toggle="modal" data-target="#myDoctor" name="" placeholder="Doctor List"  onClick={()=>this.handleModal()} />
                <Modal show={this.state.show} onHide={()=>this.handleModal()}>
                    <Modal.Header closeButton>Reassign User</Modal.Header>
                    <Modal.Body>
                    <form className="form-valide" action="#" method="post">
                        <div className="" id="myDoctor" role="dialog">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h4>Book Your Doctor</h4>
                                        <button type="button" className="close" data-dismiss="modal">X</button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="calendarbox">
                                                    <h4>Dr. Surbhi Singh</h4>
                                                    <div className="calendar"></div>
                                                </div>
                                                <div className="calendarbox">
                                                    <h4>Dr. Deepika Bhanot</h4>
                                                    <div className="calendar"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="modal fade none-border" id="event-modal">
                                            <div className="modal-dialog">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h4 className="modal-title"><strong>Add New Event</strong></h4>
                                                    </div>
                                                    <div className="modal-body"></div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                        <button type="button" className="btn btn-success save-event waves-effect waves-light">Create event</button>
                                                        <button type="button" className="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>                                       
                            </div>
                        </div>
                    </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={()=>this.handleModal()}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default Calendar;