import React, {Component} from 'react';

class CallIncoming extends Component {
  constructor(props) {
    super(props);
  }
  playAudio() {
    const audioEl = document.getElementsByClassName("audio-element")[0]
    audioEl.play()
  }

  onChangeState(){
    this.props.changeState(false);

  }
  componentDidMount() {
      this.playAudio();
  }
    render() {
        const { phone } = this.props.data;
        console.log(this.props);
        console.log(this.props.data);
        
    return (
        <div>
            <div class="callring">
                <input type="checkbox" id="open"/>
                <input type="checkbox" id="close"/>
                <label for="close" id="close-call"  onClick={this.onChangeState.bind(this)}>
                    <i class="fa fa-phone"></i>
                </label>
                
                <div id="test-test">
                    <div class="incomingcall">
                        <div class="bgd"></div>
                        <label for="open" id="open-call">
                            <i class="fa fa-phone"></i>
                        </label>
                        <div class="text">
                            <audio className="audio-element">
                                <source src="https://assets.coderrocketfuel.com/pomodoro-times-up.mp3"/>
                            </audio>
                            <div class="incoming mt-3 mb-4">Incoming call</div>
                            <p>{this.props.data}</p>
                        </div>
                    </div>
                </div>
            </div>    
            <label for="close"></label>
        </div>
        )
    }
}
CallIncoming.propTypes = {};
export default CallIncoming;