import React from "react";
import { Link } from "react-router-dom";

import TaskItem from "./TaskItem";

const ClickToCall = () => {
  
    return (
  
<div className="col-lg-3 col-md-6">
  <div className="row">
    <div className="col-md-12">
      <ul className="nav nav-tabs mb-3">
        <li className="nav-item">
          <a className="nav-link active" data-toggle="tab" href="#call1">
            Call
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" data-toggle="tab" href="#sms1">
            Chat
          </a>
        </li>
      </ul>
      <div className="tab-content">
        <div className="tab-pane fade show active" id="call1" role="tabpanel">
          <div className="dial-pad-wrap">
            <div className="left-pan">
              <div className="contacts">
                <div className="people clearfix" style={{ display: "none" }}>
                  <div className="photo pull-left">
                    <i className="mdi mdi-account-circle" />
                  </div>
                  <div className="info pull-left">
                    <div className="name">Ajay Khare</div>
                    <div className="phone">
                      <span>+91</span>
                      <span className="number" style={{ display: "none" }}>
                        844<span className="highlight">8</span>2770339
                      </span>
                    </div>
                  </div>
                </div>
                <div className="people clearfix" style={{ display: "none" }}>
                  <div className="photo pull-left">
                    <i className="mdi mdi-account-circle" />
                  </div>
                  <div className="info pull-left">
                    <div className="name">Arjun Amgain</div>
                    <div className="phone">
                      <span>+977</span>
                      <span className="number" style={{ display: "none" }}>
                        53<span className="highlight">2</span>0100325
                      </span>
                    </div>
                  </div>
                </div>
                <div className="people clearfix" style={{ display: "none" }}>
                  <div className="photo pull-left">
                    <i className="mdi mdi-account-circle" />
                  </div>
                  <div className="info pull-left">
                    <div className="name">Jane Doe</div>
                    <div className="phone">
                      <span>+977</span>
                      <span className="number" style={{ display: "none" }}>
                        000<span className="highlight">2</span>453687
                      </span>
                    </div>
                  </div>
                </div>
                <div className="people clearfix" style={{ display: "none" }}>
                  <div className="photo pull-left">
                    <i className="mdi mdi-account-circle" />
                  </div>
                  <div className="info pull-left">
                    <div className="name">John Doe</div>
                    <div className="phone">
                      <span>+977</span>
                      <span className="number" style={{ display: "none" }}>
                        7703<span className="highlight">2</span>56581
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="calling">
                <div className="name rubberBand animated">Unknown</div>
                <div className="number" />
                <div className="photo bounceInDown animated">
                  <i className="mdi mdi-account-circle" />
                </div>
                <div className="title fadeIn animated infinite">Calling...</div>
                <div className="action">
                  <div className="lnk">
                    <button className="btn fadeInLeftBig animated">
                      <i className="fa fa-mic" />
                    </button>
                  </div>
                  <div className="lnk">
                    <button className="btn fadeInLeftBig animated">
                      <i className="fa fa-vol" />
                    </button>
                  </div>
                  <div className="lnk">
                    <button className="btn fadeInRightBig animated">
                      <i className="fa fa-camera" />
                    </button>
                  </div>
                  <div className="lnk">
                    <button className="btn fadeInRightBig animated">
                      <i className="fa fa-video-camera" />
                    </button>
                  </div>
                </div>
                <div className="call-end bounceInUp animated">
                  <button className="btn">
                    <i className="fa fa-phone" />
                  </button>
                </div>
              </div>
            </div>
            <div className="dial-pad">
              <div className="dial-screen" contentEditable="false" />
              <div className="dial-table">
                <div className="dial-table-row">
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={1}>
                      <div className="dial-key">1</div>
                      <div className="dial-sub-key">&nbsp;</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={2}>
                      <div className="dial-key">2</div>
                      <div className="dial-sub-key">abc</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={3}>
                      <div className="dial-key">3</div>
                      <div className="dial-sub-key">def</div>
                    </div>
                  </div>
                </div>
                <div className="dial-table-row">
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={4}>
                      <div className="dial-key">4</div>
                      <div className="dial-sub-key">ghi</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={5}>
                      <div className="dial-key">5</div>
                      <div className="dial-sub-key">jkl</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={6}>
                      <div className="dial-key">6</div>
                      <div className="dial-sub-key">mno</div>
                    </div>
                  </div>
                </div>
                <div className="dial-table-row">
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={7}>
                      <div className="dial-key">7</div>
                      <div className="dial-sub-key">pqrs</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={8}>
                      <div className="dial-key">8</div>
                      <div className="dial-sub-key">tuv</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={9}>
                      <div className="dial-key">9</div>
                      <div className="dial-sub-key">wxyz</div>
                    </div>
                  </div>
                </div>
                <div className="dial-table-row">
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key="*">
                      <div className="dial-key">*</div>
                      <div className="dial-sub-key">&nbsp;</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key={0}>
                      <div className="dial-key">0</div>
                      <div className="dial-sub-key">+</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key="#">
                      <div className="dial-key">#</div>
                      <div className="dial-sub-key">&nbsp;</div>
                    </div>
                  </div>
                </div>
                <div className="dial-table-row no-sub-key">
                  <div className="dial-table-col"></div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key="call">
                      <div className="dial-key phonecall">
                        <i className="fa fa-phone" />
                      </div>
                      <div className="dial-sub-key">Call</div>
                    </div>
                  </div>
                  <div className="dial-table-col">
                    <div className="dial-key-wrap" data-key="back">
                      <div className="dial-key callback">
                        <i className="fa fa-long-arrow-left" />
                      </div>
                      <div className="dial-sub-key">Back</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="sms1" role="tabpanel">
          <div id="chatbox">
            <div id="friendslist">
              <div id="topmenu">
                <span className="friends" />
                <span className="chats" />
                <span className="history" />
              </div>
              <div id="friends">
                <div className="friend">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/1_copy.jpg" />
                  <p>
                    <strong>Miro Badev</strong>
                    <span>mirobadev@gmail.com</span>
                  </p>
                  <div className="status available" />
                </div>
                <div className="friend">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/2_copy.jpg" />
                  <p>
                    <strong>Martin Joseph</strong>
                    <span>marjoseph@gmail.com</span>
                  </p>
                  <div className="status away" />
                </div>
                <div className="friend">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/3_copy.jpg" />
                  <p>
                    <strong>Tomas Kennedy</strong>
                    <span>tomaskennedy@gmail.com</span>
                  </p>
                  <div className="status inactive" />
                </div>
                <div className="friend">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/4_copy.jpg" />
                  <p>
                    <strong>Enrique Sutton</strong>
                    <span>enriquesutton@gmail.com</span>
                  </p>
                  <div className="status inactive" />
                </div>
                <div className="friend">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/5_copy.jpg" />
                  <p>
                    <strong>Darnell Strickland</strong>
                    <span>darnellstrickland@gmail.com</span>
                  </p>
                  <div className="status inactive" />
                </div>
                <div id="search">
                  <input
                    type="text"
                    id="searchfield"
                    defaultValue="Search contacts..."
                  />
                </div>
              </div>
            </div>
            <div id="chatview" className="p1">
              <div id="profile">
                <div id="close">
                  <div className="cy" />
                  <div className="cx" />
                </div>
                <p>Miro Badev</p>
                <span>miro@badev@gmail.com</span>
              </div>
              <div id="chat-messages">
                <label>Thursday 02</label>
                <div className="message">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/1_copy.jpg" />
                  <div className="bubble">
                    Really cool stuff!
                    <div className="corner" />
                    <span>3 min</span>
                  </div>
                </div>
                <div className="message right">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/2_copy.jpg" />
                  <div className="bubble">
                    Can you share a link for the tutorial?
                    <div className="corner" />
                    <span>1 min</span>
                  </div>
                </div>
                <div className="message">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/1_copy.jpg" />
                  <div className="bubble">
                    Yeah, hold on
                    <div className="corner" />
                    <span>Now</span>
                  </div>
                </div>
                <div className="message right">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/2_copy.jpg" />
                  <div className="bubble">
                    Can you share a link for the tutorial?
                    <div className="corner" />
                    <span>1 min</span>
                  </div>
                </div>
                <div className="message">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/1_copy.jpg" />
                  <div className="bubble">
                    Yeah, hold on
                    <div className="corner" />
                    <span>Now</span>
                  </div>
                </div>
              </div>
              <div id="sendmessage">
                <input type="text" defaultValue="Send message..." />
                <button id="send" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-md-12">
      <div className="card">
        <div className="col-md-12">
          <h5 className="mt-4 text-muted">Agent Progress</h5>
          <div className="mt-4">
            <h4>230</h4>
            <h6>
              Total Calls <span className="pull-right">48</span>
            </h6>
            <div className="progress mb-3" style={{ height: 7 }}>
              <div
                className="progress-bar bg-primary"
                style={{ width: "30%" }}
                role="progressbar"
              >
                <span className="sr-only">Calls</span>
              </div>
            </div>
          </div>
          <div className="mt-4">
            <h4>150</h4>
            <h6 className="m-t-10 text-muted">
              Total Booking<span className="pull-right">56</span>
            </h6>
            <div className="progress mb-3" style={{ height: 7 }}>
              <div
                className="progress-bar bg-success"
                style={{ width: "50%" }}
                role="progressbar"
              >
                <span className="sr-only">Booking</span>
              </div>
            </div>
          </div>
          <div className="mt-4">
            <h4>100%</h4>
            <h6 className="m-t-10 text-muted">
              Performance<span className="pull-right">20%</span>
            </h6>
            <div className="progress mb-3" style={{ height: 7 }}>
              <div
                className="progress-bar bg-warning"
                style={{ width: "20%" }}
                role="progressbar"
              >
                <span className="sr-only">20% performance</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   
  );
};

export default ClickToCall;


       