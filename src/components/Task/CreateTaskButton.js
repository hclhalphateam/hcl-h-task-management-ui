import React from "react";
import { Link } from "react-router-dom";

const CreateTaskButton = () => {
  return (
    <React.Fragment>
      <Link to="/addIncident" className="btn btn-lg btn-info">
      <i className="fas fa-plus-circle"> </i> Create a Task
      </Link>
    </React.Fragment>
  );

  
};

export default CreateTaskButton;
