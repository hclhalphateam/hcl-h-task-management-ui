import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteTask } from "../../actions/taskActions";
import { getTask } from "../../actions/taskActions";

class TaskItem extends Component {

constructor(props){
  super();
  this.state = {
    data: this.props
  };
    this.phoneData={
      'Shiladitya bhowmik':'9769219880',
      'Vivek Dhiman':'8130578961',
      'Shailab Singh':'8826395311',
      'Durgadas Haldar':'9711393630',
      'Sanchit Singh': '8448270339'
    }
}
 onDeleteClick = id => {
    this.props.deleteTask(id);
  };

  viewDateTime=(dt)=>{
    if(dt){
    dt = dt.split("T");
    
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var d = new Date(dt[0]);
    var n = d.toLocaleDateString(['en-US'],options);

    // return `${n} @${dt[1]}`
      return `${n}`
    }
  }

  
//   updateChange = id => {

//     console.log("Id is: ", id);
//     var value = this.props.getProject(id,this.props.history);
    
//     console.log("thid krf : ",this);
//     this.props.updateFilter(this.props);
//   }

//   handleClick = (data) => {
//     console.log(data);

// }


call(number){
  // this.doCall(number);
  // console.log("%c inside the call fn", "font-size:50px; color:magenta")
  // console.log(this.props.project.projectName);
  // console.log(this.phoneData[this.props.project.projectName]);
  
  // console.log("do call methid is called usr")
  try{
      let callback_id = '347989';

      window.ameyo.integration.doDial(number,callback_id);

  }
  catch(error){
      // console.log("ameyo dial error");

      console.log(error);

  }
}
  onChangeLink(){
      this.props.changeLink(this.props.project);

    }

  render() {
    const { project } = this.props;
    // console.log("task:: ", project);

    let priorityClass;

    if (this.props.project.description === "Diabetes Care Plan") {
      priorityClass = "label label-danger";
    }

    else if (this.props.project.description === "Pre-Diabetes Care Plan") {
      priorityClass = "label label-warning";
    }

    else if (this.props.project.description === "Weight Management Care Plan") {
      priorityClass = "label label-primary";
    }
    
    else if (this.props.project.description === "HealthyHeart Care Plan") {
      priorityClass = "label label-success";
    }
    else{
      priorityClass = "label label-secondary";
    }

    return (
  // (this.props.project.currentTaskStatus === "Assigned") ?
      <li className="cursorclass"
      onClick={this.onChangeLink.bind(this)}
      >
      <div className="quick-message">
        <div className="patient-avatar">
          <div className="avatar-img">
            <i className="fa fa-user-circle-o" />
          </div>
          <span
            className="from-account"
            style={{ backgroundColor: "hotpink" }}
          />
        </div>
        <div className="message">        
          <h5>{project.projectName}</h5>
          <p>
            <b>Pan Id:</b> {project.projectIdentifier}, <b>Employee ID:</b> {project.employeeId}
          </p>
          <span className={`${priorityClass}`}>
          {project.description}
          {/* {new Date()} */}
          </span>
        </div>
        <div className="message-edit">
          <div className="rating">
            <div className="ratingpoints">
              <span className="rating-num">3.0</span>
              <div className="calldetail">
              <div className="dial-pad">
                <div className="dial-screen" >
                  <div className="info pull-left">
                    <div className="phone">+91 8448270339</div>
                  </div>
                </div>
              </div>
              <div className="dial-table-col">
                <div className="dial-key-wrap" data-key="call">
                  <div className="dial-key phonecall">
                    <i className="fa fa-phone-square" onClick={
                  ()=>this.call(project.patientMobile)
                  }/>
                  </div>
                </div>
              </div>
            </div>
            </div>
            {/* <div className="rating-stars">
              <span className="fa fa-star checked" />
              <span className="fa fa-star checked" />
              <span className="fa fa-star checked" />
              <span className="fa fa-star" />
              <span className="fa fa-star" />
            </div> */}
          </div>
          <div className="edit_cross">
            {/* <Link to={`/myTasksBoard/${project.taskId}`}>
            <i className="fa fa-flag-checkered pr-4">  </i>
            </Link> */}
            <Link to={`/updateIncident/${project.taskId}`}>
              <i className="fa fa-edit"> </i>
            </Link>
            <i onClick={this.onDeleteClick.bind(
                this,
                project.taskId
              )}
              className="fa fa-minus-circle">
            </i>
            {this.viewDateTime(project.assignedDate)}
          </div>
        </div>
        {/* <Link to={`/myTasksBoard/${project.projectIdentifier}`}>
              <li className="list-group-item board">
                <i className="fa fa-flag-checkered pr-2">  </i> <b> MyTasks Board</b>
              </li>
            </Link>
            <Link to={`/updateIncident/${project.projectIdentifier}`}>
              <li className="list-group-item update">
                <i className="fa fa-edit pr-1"> </i><b> Update Incident</b>
              </li>
            </Link>
          <li className="list-group-item delete"
            onClick={this.onDeleteClick.bind(
              this,
              project.projectIdentifier
            )}
          >
            <i className="fa fa-minus-circle pr-1"> </i>
            <b> Delete Incident</b>
          </li> */}
      </div>
    </li> 
    // :
    //     ''
    
      // <div className="container">
         
        // <div className="card card-body bg-light mb-3">
        //   <div className="row">

        //     <div className="col-2">
        //       <span className="mx-auto"><h6><b>PAN ID:</b></h6> {project.projectIdentifier}
             
        //       </span>
        //     </div>
        //     <div className="col-lg-4 col-md-4 col-8"> 
        //       <h4>{project.projectName}</h4>
        //       <p><b>Employee ID:</b> {project.projectIdentifier}</p>
        //       <span className="label1 label-danger">{project.description}</span>
              
              
        //     </div>
        //     <div className="col-md-6 d-none d-lg-block">
        //       <ul className="list-group">
        //         <Link to={`/myTasksBoard/${project.projectIdentifier}`}>
        //           <li className="list-group-item board">
        //             <i className="fa fa-flag-checkered pr-2">  </i> <b> MyTasks Board</b>
        //           </li>
        //         </Link>
        //         <Link to={`/updateIncident/${project.projectIdentifier}`}>
        //           <li className="list-group-item update">
        //             <i className="fa fa-edit pr-1"> </i><b> Update Incident</b>
        //           </li>
        //         </Link>

        //         <li
        //           className="list-group-item delete"
        //           onClick={this.onDeleteClick.bind(
        //             this,
        //             project.projectIdentifier
        //           )}
        //         >
        //           <i className="fa fa-minus-circle pr-1"> </i>
        //           <b> Delete Incident</b>
        //         </li>
        //       </ul>
        //     </div>

        //   </div>
        // </div>
  
    );
  }
  
}

TaskItem.propTypes = {
  deleteTask: PropTypes.func.isRequired,
  getTask: PropTypes.func.isRequired,
  updateFilter: PropTypes.func
};

export default connect(
  null,
  { deleteTask,getTask }
)(TaskItem);
