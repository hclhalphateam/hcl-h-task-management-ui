import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteTaskwoConfirm } from "../../actions/taskActions";
import { getTask, createTask } from "../../actions/taskActions";
import swal from 'sweetalert';

import axios from 'axios';
import { data } from "jquery";
import Calendar from  '../Calendar1'
// import { Pagination, Table } from 'semantic-ui-react';
import { useState, useEffect } from 'react';

// import Pagination from "react-js-pagination";
import PaginationSMS from './PaginationSMS';
// import {JSON} from 'json';

class TaskPane extends Component {
  
  constructor(props) {

    super(props) ;

    this.state = {  
      activePage: 15,
      pageLimit : 10,
      urlSearchParams : new URLSearchParams(window.location.search),
      // pageParam : Number(this.state.urlSearchParams.get("page")) || 1,
      page : 1,
      //this.state.pageParam,
      setPage : 1,
      // this.state.pageParam,
      dataList : [],
      setData: [],
      total : 0,
      setTotal : 0,
      
      sms_sender_id:"HCLSMS",
      sms_source:"DSP Platform",
      sms_creation_date: new Date(),
      sms_template_id:0,
      sms_request_data:{},
      sms_body:'',
      destination_mobile_no: '',
      DoctorId:'',  
      FacilityId: '',  
      FacilityData: [], 
      ServiceData:[], 
      SMSData:[],
      DoctorData: [],
      doctorDate: '',
      dieticianDate: '',
      sampleDate: '',
      patientId: '',
      activePage:1,
      patientName: '',
      doctorName: '',
      disposition: '',
      consultationType: '',
      remarks: '',
      patient_location: '',
      currentTaskStatus: '',
      services:[],
      stage_timeline:'',
      currentAssignee: '',
      taskId:'',
      preferred_lang:'',
      provider_location:'',
      nextfollowupDate: '',
      phccallbackDate: '',
      centername:'',
      utilizationDate:'',
      utilizationstatus:'',
      taskid:'',
      tasknotesubject:"",
      tasknotemsg:"",
      pageOfItems: [],
      tasknotes:[],
      errors: {}
    };
    this.phoneData={
      'Shiladitya bhowmik':'9769219880',
      'Vivek Dhiman':'8130578961',
      'Shailab Singh':'8826395311',
      'Durgadas Haldar':'9711393630',
      'Sanchit Singh': '8448270339'
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onClick = this.onClick.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);

    // this.onSMSClick = this.onSMSClick.bind(this);
    this.onNotesClick = this.onNotesClick.bind(this);
    this.onNotesCreateClick = this.onNotesCreateClick.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
  }

  onChangePage(pageOfItems) {
    // update state with new page of items  
    this.setState({ pageOfItems: pageOfItems });
  }
  handlePageChange(pageNumber) {
    // console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }
  secondsDiff(d1, d2) {
    let millisecondDiff = d2 - d1;
    let secDiff = Math.floor( ( d2 - d1) / 1000 );
    return secDiff;
  }
  minutesDiff(d1, d2) {
    let seconds = this.secondsDiff(d1, d2);
    let minutesDiff = Math.floor( seconds / 60 );
    return minutesDiff;
  }
  hoursDiff(d1, d2) {
    let minutes = this.minutesDiff(d1, d2);
    let hoursDiff = Math.floor( minutes / 60 );
    return hoursDiff;
  }
  daysDiff(d1, d2) {
    let hours = this.hoursDiff(d1, d2);
    let daysDiff = Math.floor( hours / 24 );
    return daysDiff;
  };
  formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }
  //life cycle hooks
  componentWillReceiveProps(nextProps) {
    this.setState({    tasknotes : nextProps.notesdata});
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  openCalendar(pan,patient_name){
    console.log(pan);
    console.log(patient_name);
    window.open('https://hcl-h-cal.web.app/?pan='+pan+'&patient_name='+patient_name+'','_blank','width=800, height=900'); return false;  

  } 
  onNotesClick(e){
    e.preventDefault();
    axios.get('https://task-dsp.hclhealthcare.in:8080/v1/tasknotebypan?pan='+this.props.data.projectIdentifier
    )
    .then(res => {
      const tasknotes = res.data;
      this.setState({    tasknotes });
    });    
  }

  onNotesCreateClick(e){
    e.preventDefault();

    const notesbody = {
      "taskId": this.props.data.taskId,
      "noteDesc": this.state.tasknotemsg,
      "noteMetadata": "",
      "createdTime": new Date(),
      "createdByAgentId": this.props.data.tenantId,
      "createdByAgentName": this.props.data.tenantName,
      "noteTitle": this.state.tasknotesubject,
      "pan": this.props.data.projectIdentifier
    }
     axios.post('https://task-dsp.hclhealthcare.in:8080/v1/tasknote/add', 
     notesbody
          )
          .then((response) => {

            console.log(response);

            let tasknotes = [...this.state.tasknotes]
            tasknotes.push(notesbody)
            // setting back the state will trigger a rerender and will show the changes on screen
            this.setState({tasknotes})
            this.setState({tasknotesubject:""})
            this.setState({tasknotemsg:""})
            
            swal("Note Created Successfully", 
            "", "")
          }, (error) => {
            console.log(error);
          });    
          // setTimeout(function(){window.location.reload();},2000);

        }


  onClick(e){
    e.preventDefault();

    const smsBody = {
      sms_sender_id:this.state.sms_sender_id,
      sms_source:this.state.sms_source,
      sms_creation_date:this.state.sms_creation_date,
      sms_template_id:this.state.sms_template_id,
      sms_request_data:this.state.sms_request_data,
      sms_body:this.state.sms_body,
      destination_mobile_no: this.props.data.patientMobile

    };
          console.log(smsBody);
          axios.post('https://scheduling-dsp.hclhealthcare.in:8086/v1/sendSMS', 
          smsBody
          )
          .then((response) => {

            console.log(response);
            let pageOfItems = [...this.state.pageOfItems]
            pageOfItems.push(smsBody)
            // setting back the state will trigger a rerender and will show the changes on screen
            this.setState({pageOfItems})
            this.setState({sms_body:""})
            
        //      this.setState({
        //     destination_mobile_no: this.props.data.patientMobile
        // })
            swal("SMS Sent Successfully", 
            "", "")
          }, (error) => {
            console.log(error);
          });

          
      // window.document.getElementById("textsmsbody").reset();
      // setTimeout(function(){window.location.reload();},15);
      // setTimeout(function(){window.location.reload();},2000);

  }

   onSubmit(e) {
    e.preventDefault();

    const newAppointment = {
      // projectName: this.state.projectName,
      // projectIdentifier: this.state.projectIdentifier,
      // description: this.state.description,
      // start_date: this.state.start_date,
      // end_date: this.state.end_date,

      startTime: this.state.startTime,
      endTime: this.state.endTime,
      doctorName: this.state.doctorName,
      disposition: this.state.disposition,
      consultationType: this.state.consultationType,
      remarks: this.state.remarks,
      patient_location: this.state.patient_location,
      currentTaskStatus: this.state.currentTaskStatus,
      currentAssignee: this.state.currentAssignee,
      task_id:this.props.data.taskId,
      preferred_lang:this.state.preferred_lang,
      taskId:this.props.data.taskId,
      provider_location:this.state.provider_location,
      centername:this.state.centername,
      doctorDate: this.state.doctorDate,
      dieticianDate: this.state.dieticianDate,
      sampleDate: this.state.sampleDate,
      nextfollowupDate: this.state.nextfollowupDate,
      phccallbackDate: this.state.phccallbackDate,
      services:this.state.services

    };
    // this.props.createTask(newAppointment, this.props.history);


    axios.post('https://task-dsp.hclhealthcare.in:8080/v1/taskentity', 
    newAppointment
    ) 
    .then( async (response) => {
      console.log("Response appointment:", response.data);
      // this.setState({  
      //           }); 
      swal("Form Submitted Successfully", 
      "", "")
      console.log(this.props.packageid);

    let params = {
      "pan":this.props.data.projectIdentifier,
      "package_id" :this.props.packageid,
    };

    let query = Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&');

    const taskobj = {
      pan: this.props.data.projectIdentifier,
      projectName: this.props.data.projectName,
      projectIdentifier: this.props.data.projectIdentifier,
      description: this.props.data.description,
      age: this.props.data.age,
      gender: this.props.data.gender,
      patientMobile: this.props.data.patientMobile,
      employer: this.props.data.employer,
      billNumber: this.props.data.billNumber,
      facilityName: this.props.data.facilityName,
      packageName: this.props.data.packageName,
      employeeId: this.props.data.employeeId,
      emailID: this.props.data.emailID,
    }

      await axios.post('https://task-dsp.hclhealthcare.in:8080/v1/createtimeline?'+query, 
        taskobj
      ) 
      .then((response) => {
        console.log("New Task Data:", response.data);
        // this.setState({
        //   taskid: response.data.body.taskId
        // })
        // console.log("Task id passed: ", this.state.taskid );
      })

      // console.log("Task id is: ", this.state.taskid);

      // const obj = { 'taskId': this.props.data.taskId,
      // 'serviceName': "Glucometer based RBS",
      // 'utilizationDate': this.state.utilizationDate,
      // 'utilizationStatus': this.state.utilizationstatus};
    
      // axios.post('https://task-dsp.hclhealthcare.in:8080/api/service/create', 
      // obj
      // ) 
      // .then((response) => {
      //   console.log("Service Data:", response.data);
      // })
      
    //   fetch(
    //       'https://task-dsp.hclhealthcare.in:8080/api/updatetask/'+this.props.data.taskId,
    //       {
    //       method:'PUT'
    // }).then(response => response.json())
    // .then(json=> console.log(json)) 
    
    // console.log(JSON.parse(JSON.stringify(this.props.data.services)));
    

      // this.props.deleteTaskwoConfirm(this.props.data.taskId);

      document.getElementById("taskpaneform").reset();
      // setTimeout(function(){window.location.reload();},15);
      setTimeout(function(){window.location.reload();},2000);

    }, (error) => {
      console.log(error);
    });
  }
    componentDidMount() {       
    
    var setRecordForShowCrmCallback = function(response) {
      if (response.result) {
        console.log("%c crm callback response: ", "font-size:48px; color:pink")
        console.log(response)
        // var crmPage = document.getElementById('crmPage');
        // var html = "<p>" + "Response : SetRecordShowCrm ->"
        //     + response.result.status + "</p>";
        // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
    
      }
    };
    let vm = this;
    function customShowCrm(phone, additionalParams, requestId) {     
      console.log("%cwindow.ameyo event listener message: ", "font-size:48px; color:blue");
      console.log(phone );
      console.log(additionalParams);			
      console.log(requestId);	
      console.log(additionalParams.callType!==undefined);
      console.log(typeof additionalParams);

      let jsonAdditionalPrarams= JSON.parse(additionalParams);
      console.log(jsonAdditionalPrarams.callType);
      if(jsonAdditionalPrarams.callType!==undefined && jsonAdditionalPrarams.callType==="inbound.call.dial"){

        // vm.setState({phoneincoming:true});
        // vm.setState({incomingphone:phone});
        vm.props.togglephoneincoming(true);
        vm.props.emitincomingphone(phone);
     
      } 

      console.log(vm.props.data.projectIdentifier);
      const callEventBody ={
          callID: "",
          requestID: requestId,
          pan: vm.props.data.projectIdentifier,
          queueId: jsonAdditionalPrarams.queueId,
          dstPhone: jsonAdditionalPrarams.dstPhone,
          campaignId: jsonAdditionalPrarams.campaignId,
          associationType: jsonAdditionalPrarams.associationType,
          sessionId: jsonAdditionalPrarams.sessionId,
          userId: jsonAdditionalPrarams.userId,
          callType: jsonAdditionalPrarams.callType,
          crm_push_generated_time: jsonAdditionalPrarams.crm_push_generated_time,
          userCrtObjectId: jsonAdditionalPrarams.userCrtObjectId,
          queueName: jsonAdditionalPrarams.queueName,
          phone: phone,
          crtObjectId: jsonAdditionalPrarams.crtObjectId,
          inactive: false
      }

      axios.post('https://task-dsp.hclhealthcare.in:8083/v1/insertcallevent', 
        callEventBody
        )
        .then((response) => {
          console.log(response);
          // swal("Call Event Inserted Successfully", 
          // "", "")
        }, (error) => {
          console.log(error);
        });
        console.log(vm.state.incomingphone);
        console.log(vm.state.phoneincoming);
        window.ameyo.integration.api.setRecordInfoForShowCrm(jsonAdditionalPrarams.memberUserCRTObjectId, requestId,"wedwef","refgrggtr", phone,
          setRecordForShowCrmCallback);
      }
    function customCallDispose(userCustomerCRTInfo) { 
      console.log("%cwindow.ameyo event:: call dispose: ", "font-size:48px; color:orange");
      console.log(userCustomerCRTInfo);	
      // alert(userCustomerCRTInfo);
    }
    function handleHangup(reason, userCustomerCRTInfo){         
      console.log("%cwindow.ameyo event:: call dispose: ", "font-size:48px; color:orange");
      console.log(reason);	
      console.log(typeof userCustomerCRTInfo);	

      vm.props.togglephoneincoming(false);
      // vm.setState({phoneincoming:false});
      
      console.log(vm.state.phoneincoming);
    }

    function showCrmDetailedCustom(recordInfo) {
      // var crmPage = document.getElementById('crmPage');
      // var html = "Record Id : " + recordInfo.recordId + "<br> User CRT info : "
      //         + recordInfo.userCustomerCRTInfo.userCrtObjectId
      //         + "<br> Customer CRT Info : "
      //         + recordInfo.userCustomerCRTInfo.customerCrtObjectId
      // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
      console.log("%c showCrmDetailedCustom message: ", "font-size:48px; color:grey");
      console.log(recordInfo.recordId);
      window.alert(recordInfo.recordId);
    }

    function handleOnLoad() {
      // var crmPage = document.getElementById('crmPage');
      // var html = "On Load";
      // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
      console.log("%c onload message: ", "font-size:48px; color:grey");
  }
function handleConferWithUser(reason, userCustomerCRTInfo) {
  // var crmPage = document.getElementById('crmPage');
  // var html = "Confer With User : " + reason + "<br> User CRT info : "
  //         + userCustomerCRTInfo.userCrtObjectId + "<br> Customer CRT Info : "
  //         + userCustomerCRTInfo.customerCrtObjectId;
  // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
  console.log("%c handleconferwithuser message: ", "font-size:48px; color:grey");

  console.log(reason);

  console.log(userCustomerCRTInfo);
}
    
    let customIntegration = {};
    customIntegration.showCrm = customShowCrm;
    customIntegration.hangupHandler = handleHangup;
    customIntegration.showCrmDetailed = showCrmDetailedCustom;
    customIntegration.handleDisposeCall = customCallDispose;
    customIntegration.onLoadHandler = handleOnLoad;
    customIntegration.conferWithUserHandler = handleConferWithUser;

   window.ameyo.integration.registerCustomFunction("showCrm", customIntegration);
   window.ameyo.integration.registerCustomFunction("loginHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("logoutHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("onLoadHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("loginStatusHandler",
        customIntegration);
   window.ameyo.integration
        .registerCustomFunction("forceLoginHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("selectExtensionHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("modifyExtensionHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("selectCampaignHandler",
        customIntegration);
   window.ameyo.integration
        .registerCustomFunction("autoCallOnHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("autoCallOffHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("readyHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("breakHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("hangupHandler", customIntegration);
   window.ameyo.integration.registerCustomFunction("transferToPhoneHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("transferInCallHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("transferToAQHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("transferToIVRHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("transferToUserHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("transferToCampaignHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("conferWithPhoneHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("conferWithTPVHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("conferWithUserHandler",
        customIntegration);
   window.ameyo.integration.registerCustomFunction("conferWithLocalIVRHandler",
        customIntegration);
   window.ameyo.integration
        .registerCustomFunction("handleDisposeCall", customIntegration);
   window.ameyo.integration.registerCustomFunction("showCrmDetailed", customIntegration);
  

                            // console.log(this.props.data.services);
                    axios.post('https://exit-dsp.hclhealthcare.in:5001/v1/api/facilities', 
                    {
                      "data_type": "FACILITIES",
                      "optional_search_filters":[
                      {
                      "type"  : "",
                      "value" : ""
                      }
                      ]
                    }
                    )
                    
                    .then((response) => {
                      console.log("Response :", response.data.data);
                      this.setState({  
                                        FacilityData: response.data.data

                                }); 

                    console.log("FacilityData is :", this.state.FacilityData)

                    }, (error) => {
                      console.log(error);
                    });
            
                  }  
            ChangeteState = (e) => {
              //   console.log("value is:", e.target.value)
              this.setState({ [e.target.name]: e.target.value, FacilityId: e.target.value});
              axios.post('https://exit-dsp.hclhealthcare.in:5001/v1/api/providers', {
                "data_type": "DOCTORS",
                "optional_search_filters":[
                  {
                  "type"  : "facility_id",
                  "value" : e.target.value
                  }
                ]
              }
            )
            .then((response) => {
              console.log("Doctor data ", response.data.data);
              
              // console.log("Doctor Id ", response.data.data.provider_id);
              this.setState({  
                DoctorId: response.data.data.provider_id,
                DoctorData: response.data.data  
              });
            }, 
            (error) => {
              console.log(error);
            });
          }
          render() {
          // console.log("Task in task pane: ",this.props.data);
          return (
            <div className="row">
              <ul className="nav nav-tabs mb-3" role="tablist">
                <li className="nav-item">
                  <a className="nav-link active show" data-toggle="tab" href="#taskpane">
                    Task Pane
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#profile2">
                    Profile
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#notestab">
                    Notes
                  </a>
                </li>
                {/* <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#home">
                    Emails
                  </a>
                </li> */}
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#lifecycle">
                    Lifecycle
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#chat">
                    SMS
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#videoCall">
                    Video Call
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#portals">
                    Portals
                  </a>
                </li>
              </ul>
            <div className="col-md-12">
              <div className="card">     
                <div className="viewnotefunction">
                  <span
                    className="fa fa-pencil-square-o"
                    aria-hidden="true"
                    data-toggle="modal"
                    data-target="#addnote"
                  />
                  <div className="modal fade" id="addnote" role="dialog" aria-hidden="false">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">
                            Patient Notes
                          </h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">x</span>
                          </button>
                        </div>
                        <div className="modal-main" id="modalnote">
                          <div className="card-body">
                            <ul className="nav nav-tabs tab-below tabs" role="tablist">
                              {/* <li className="nav-item">
                                <a
                                  className="nav-link active show"
                                  data-toggle="tab"
                                  href="#viewnote"
                                  role="tab"
                                >
                                  View Note
                                </a>
                              </li> */}
                              <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" href="#addnote" role="tab"> 
                                  Add Note
                                </a>
                              </li>
                            </ul>
                            {/* <div className="tab-content tabs card-block">
                            </div> */}
                          </div>
                          <div className="tab-pane" id="addnote" role="tabpanel">
                            <div className="col-md-12">
                              <div className="card-body d-flex flex-column">
                                <h4 style={{ textAlign: "left" }}>Add Note</h4>
                                <form className="quick-post-form">
                                  <div className="form-group">
                                    <input
                                      type="text"
                                      className="form-control"
                                      id="exampleInputText1"
                                      aria-describedby="textHelp"
                                      placeholder="subject"
                                      name="tasknotesubject" 
                                      value={this.state.tasknotesubject}
                                      onChange={this.onChange} 
                                    />
                                  </div>
                                  <div className="form-group">
                                    <textarea className="form-control" placeholder="Enter your message..."
                                      name="tasknotemsg" value={this.state.tasknotemsg}
                                      onChange={this.onChange} 
                                    />
                                  </div>
                                  <div className="form-group mb-0">
                                    <button type="submit" className="btn btn-primary" onClick={this.onNotesCreateClick}>
                                      Create
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                            {/* Nav tabs */}
                          </div>
                        </div>
                      
                  </div>
                </div>
                <div className="tab-content">
          
                  <div className="tab-pane fade active show" id="taskpane">
                  <div class="col-md-12">
                  <div class="lifecycle">

                    {/* <h4 class="text-center">Task Status</h4> */}
                  {/* <p><b>Agent Name: </b>{this.props.data.tenantName} </p> */}
                  {/* <p><b>Agent Id: </b>{this.props.data.tenantId}</p> */}
                  {/* <p><b>Stage: </b>{this.props.data.stage_timeline}</p>
                  <p><b>Services: </b>{this.props.data.services}</p> */}
                  <p><b>Service: </b>
                  {this.props.servicedata && this.props.servicedata.length ? this.props.servicedata.map((e, key) => {  
                            // console.log(e);
                  return (
                    <p>{e}</p>
                    );
                      })
                      :''
                  }
                  </p>
                  <p><b>ServiceType:</b> {this.props.data.stage_timeline}</p>
                  <p><b>PAN: </b>{this.props.data.projectIdentifier}</p>
                  </div></div>
                    <div className="col-md-12">
                      <form id="taskpaneform" className="form-details" onSubmit={this.onSubmit}>
                        <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                            Disposition:
                          </label>
                          <div className="col-lg-8">
                            <select className="form-control input-default" name="disposition" 
                              value={this.state.disposition}
                              onChange={this.onChange}  
                            >
                              <option value="Select" className="select">
                                Select
                              </option>
                              {/* <option value={0}>Appointment Confirmed</option>
                              <option value={1}>Appointment Rescheduled</option>
                              <option value={2}>Call Back Later</option>
                              <option value={3}>Call Didn't Connect</option>
                              <option value={4}>Patient Didn't Connect</option>
                              <option value={5}>Not Interested</option>
                              <option value={6}>Left HCL or Not eligible</option>
                              <option value={7}>Left Care Plan</option>
                              <option value={8}>Cancel</option>
                    */}
                              <option value="Left HCL/Not Eligible/Deceased">Left HCL/Not Eligible/Deceased</option>
                              <option value="Visit Completed">Visit Completed</option> 
                              <option value="No Response/Switched Off/Disconnected/Not Reachable/Not Connecting/Wrong Number">No Response/Switched Off/Disconnected/Not Reachable/Not Connecting/Wrong Number</option>
                              <option value="Call Back Later">Call Back Later</option>
                              <option value="Information Provided">Information Provided</option>
                              <option value="Escalation Raised">Escalation Raised</option>
                              <option value="No Show/Consult Pending">No Show/Consult Pending</option> 
                              <option value="Sample booked">Sample booked</option> 
                              <option value="Consult Booked">Consult Booked</option>
                            </select>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                            Provider's Location:
                          </label>
                          <div className="col-lg-8">

                            
                          <select  className="form-control input-default"
                              id="select-locations"  name="country" value={this.state.FacilityId} 
                              onChange={this.ChangeteState} 
                              name="provider_location" 
                              required
                          >

                          <option>Select Location</option>  


                          {this.state.FacilityData.map((e, key) => {  
                            // console.log(e);
                                  return <option key={key} value={e.facility_id}>{e.address}</option>;  
                          })}  

                          </select>  
                      
                            {/* <select
                              className="form-control input-default"
                              id="select-locations"
                            >
                              <option value>Select Location</option>
                              <option value="html">Noida 126</option>
                              <option value="css">Noida 24</option>
                              <option value="javascript">Bengaluru</option>
                              <option value="angular">Madurai</option>
                              <option value="angular">Chennai</option>
                              <option value="vuejs">Lucknow</option>
                              <option value="ruby">Gurgaon</option>
                            </select> */}
                          </div>
                        </div>
                        
                        <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                          CC Center Name
                          </label>
                          <div className="col-lg-8">                      
                          <select  className="form-control input-default"
                              id="select-locations"  name="centername" value={this.state.centername} 
                            onChange={this.onChange}
                              
                          >

                              <option value>Select Center</option>
                              <option value="Care Cordinator SEZ Noida">Care Coordinator SEZ Noida</option>
                              <option value="Care Cordinator Noida 24">Care Coordinator Noida 24</option>
                              <option value="Care Cordinator Hyderabad">Care Coordinator Hyderabad</option>
                              <option value="Care Cordinator Chennai">Care Coordinator Chennai</option>
                              <option value="Care Cordinator Maduari">Care Coordinator Madurai</option>
                              {/* <option value="Care Cordinator Maduari">Care Coordinator Maduari</option> */}
                              <option value="Care Cordinator Lucknow">Care Coordinator Lucknow</option>
                              <option value="Care Cordinator Gurgaon">Care Coordinator Gurgaon
                          </option>
                              
                            </select>
                    
                          </div>
                        </div>
                        {/* <button
                                                type="button"
                                                className="fc-prev-button fc-button fc-state-default fc-corner-left"
                                                aria-label="prev"
                                              >Check Availability
                                                <span className="fc-icon fc-icon-left-single-arrow" />
                                              </button> */}
                        {/* <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                            Doctor's Name:
                          </label>
                          <div className="col-lg-8">

                          <select className="form-control input-default slct" name="doctorName" id="select_doctors"

                          value={this.state.doctorName}
                          onChange={this.onChange}  required

                          >  

                          <option>Select Doctor</option> 

                                  {this.state.DoctorData.map((e, key) => {  
                                          return <option key={key}    data-toggle="modal"
                                          data-target="#myDoctor" value={e.provider_id}>{e.provider_name}</option>;  
                                  })}  
                          </select>  
                          
                          </div>
                        </div> */}
                      
                      

                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Check Doctor Availability
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              {/* <input
                                type="text"
                                className="form-control input-default"
                                placeholder="Doctor's List"
                                name="patient_location"
                                value={this.state.patient_location}
                                onChange={this.onChange}
                                
                              /> */}
                              <div className="card"><a href='javascript:void(0)'  onClick={e =>this.openCalendar(this.props.data.projectIdentifier,this.props.data.projectName)}> Get Doctor Availability </a></div>
                                {/* <Calendar/> */}
                            </div>
                          </div>
                        </div>
                    

                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Start Time
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="doctorDate"
                                value={this.state.startTime}
                                onChange={this.onChange}
                                
                              />
                              {/* <span className="input-group-append">
                                <span className="input-group-text">
                                  <i className="mdi mdi-calendar-check" />
                                </span>
                              </span> */}
                            </div>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            End Time
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="endTime"
                                value={this.state.endTime}
                                onChange={this.onChange}
                                
                              />
                              {/* <span className="input-group-append">
                                <span className="input-group-text">
                                  <i className="mdi mdi-calendar-check" />
                                </span>
                              </span> */}
                            </div>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Dietician Appointment Date
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="dieticianDate"
                                value={this.state.dieticianDate}
                                onChange={this.onChange}
                                
                              />
                              {/* <span className="input-group-append">
                                <span className="input-group-text">
                                  <i className="mdi mdi-calendar-check" />
                                </span>
                              </span> */}
                            </div>
                          </div>
                        </div>

                        
                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                        Sample Collection Date
                        
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="sampleDate"
                                value={this.state.sampleDate}
                                onChange={this.onChange}
                                
                              />
                              {/* <span className="input-group-append">
                                <span className="input-group-text">
                                  <i className="mdi mdi-calendar-check" />
                                </span>
                              </span> */}
                            </div>
                          </div>
                        </div>
                        
                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-suggestions"
                          >
                            Appointment Type
                          </label>
                          <div className="col-lg-8">
                            <div className="custom-control custom-radio custom-control-inline">
                              <input
                                type="radio"
                                id="customRadioInline1"
                                name="consultationType"
                                className="custom-control-input"
                                
                          value="Sample"
                          onChange={this.onChange}
                          
                          />
                              <label
                                className="custom-control-label"
                                htmlFor="customRadioInline1"
                              >
                                Sample
                              </label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                              <input
                                type="radio"
                                id="customRadioInline2"
                                name="consultationType"
                                className="custom-control-input"
                                
                          value="Consult"
                          onChange={this.onChange}
                          
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="customRadioInline2"
                              >
                                Consult
                              </label>
                            </div>
                            {/* <div className="custom-control custom-radio custom-control-inline">
                              <input
                                type="radio"
                                id="customRadioInline3"
                                name="consultationType"
                                className="custom-control-input"
                                required
                          value={this.state.consultationType}
                          onChange={this.onChange}
                          
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="customRadioInline3"
                              >
                                Audio Consultation
                              </label>
                            </div> */}
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                          Preferred Language
                          </label>
                          <div className="col-lg-8">

                            
                          <select  className="form-control input-default"
                              id="select-locations"  name="preferred_lang" value={this.state.preferred_lang} 
                              onChange={this.onChange}
                              
                          >
                              <option value>Preferred Language</option>
                              <option value="English">English</option>
                              <option value="Hindi">Hindi</option>
                              <option value="Tamil">Tamil</option>
                              <option value="Telugu">Telugu</option>
                              <option value="Kannada">Kannada</option>
                              <option value="Malayalam">Malayalam</option>
                              <option value="Bengali">Bengali</option>
                              
                            </select>
                    
                          </div>
                        </div>


                        <div className="form-group row">
                          <label className="col-lg-4 col-form-label" htmlFor="val-doctor">
                          Patient Location
                          </label>
                          <div className="col-lg-8">

                            
                          <select  className="form-control input-default"
                              id="select-locations"   name="patient_location"
                              value={this.state.patient_location}
                            onChange={this.onChange}
                              
                          >
                            <option value>Patient Location</option>
                              {/* <option value="Care Cordinator SEZ Noida">Noida 126</option> */}
                              <option value="Noida">Noida</option>
                              <option value="Hyderabad">Hyderabad</option>
                              <option value="Chennai">Chennai</option>
                              <option value="Maduari">Madurai</option>
                              <option value="Lucknow">Lucknow</option>
                              <option value="Gurgaon">Gurgaon
                              </option>
                            </select>
                    
                          </div>
                        </div>

                        {/* <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Patient Location
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="text"
                                className="form-control input-default"
                                placeholder="Location"
                                name="patient_location"
                                value={this.state.patient_location}
                                onChange={this.onChange}
                                
                              />
                            
                            </div>
                          </div>
                        </div> */}
                      {this.props.servicedata && this.props.servicedata.length?
                        <div className="form-group row">
                  <table id="servicetable" className="tableclass center">
                  <tr>
                    <th>Service Name</th>
                    <th>Utilization Date</th>
                    <th>Utilization Status</th>
                  </tr>
                    
                    {this.props.servicedata && this.props.servicedata.length ?this.props.servicedata.map((e, key) => {  
                            console.log(e);
                  return (
                  <tr>
                    <td>{e}</td>

                    <td> <input
                                type="date"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="utilizationDate"
                                onChange={this.onChange}
                                value={this.state.utilizationDate}
                                /></td>           

                    <td>
                    <select  className="form-control input-default"
                              id="select-locations"  name="utilizationstatus"  
                              onChange={this.onChange} value={this.state.utilizationstatus}
                              
                          >
                              
                              <option value>Status</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                              <option value="Patient Denied">Patient Denied</option>
                              <option value="Service Unavailable">Service Unavailable</option>
                              
                            </select>

                    </td>
                    </tr>);
                      })
                      :''
                      }  
                    </table>
                    </div>
                    :''
                    }

                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Next FollowUp Date
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="nextfollowupDate"
                                value={this.state.nextfollowupDate}
                                onChange={this.onChange}
                                
                              />
                            
                            </div>
                          </div>
                        </div>




                        <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-confirm-password"
                          >
                            Next PHC Callback Date
                          </label>
                          <div className="col-lg-8">
                            <div className="input-group">
                              <input
                                type="datetime-local"
                                className="form-control mydatepicker input-default"
                                placeholder="mm/dd/yyyy"
                                name="phccallbackdate"
                                value={this.state.phccallbackdate}
                                onChange={this.onChange}
                                
                              />
                            
                            </div>
                          </div>
                        </div>

                        {/* <div className="form-group row">
                          <label
                            className="col-lg-4 col-form-label"
                            htmlFor="val-currency"
                          >
                            Remarks
                          </label>
                          <div className="col-lg-8">
                            <textarea
                              className="form-control input-default"
                              id="exampleFormControlTextarea1"
                              rows={3}
                              defaultValue={""}
                              name="remarks"
                              value={this.state.remarks}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </div> */}
                        <div className="form-group row">
                          <div className="col-lg-8 ml-auto">
                            <button type="submit" className="btn btn-primary">
                              Submit
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="home">
                    <div className="email-right-box col-md-12">
                      <div className="email-center-box">
                        <a
                          href="email-compose.html"
                          className="btn btn-primary btn-block"
                        >
                          Compose
                        </a>
                        <div className="mail-list">
                          <a
                            href="email-inbox.html"
                            className="list-group-item border-0 text-primary p-r-0"
                          >
                            <i className="fa fa-inbox font-18 align-middle mr-2" />{" "}
                            <b>Inbox</b>{" "}
                            <span className="badge badge-primary badge-sm float-right m-t-5">
                              198
                            </span>{" "}
                          </a>
                          <a href="#" className="list-group-item border-0 p-r-0">
                            <i className="fa fa-paper-plane font-18 align-middle mr-2" />
                            Sent
                          </a>{" "}
                          <a href="#" className="list-group-item border-0 p-r-0">
                            <i className="fa fa-star-o font-18 align-middle mr-2" />
                            Important{" "}
                            <span className="badge badge-danger badge-sm float-right m-t-5">
                              47
                            </span>{" "}
                          </a>
                          <a href="#" className="list-group-item border-0 p-r-0">
                            <i className="mdi mdi-file-document-box font-18 align-middle mr-2" />
                            Draft
                          </a>
                          <a href="#" className="list-group-item border-0 p-r-0">
                            <i className="fa fa-trash font-18 align-middle mr-2" />
                            Trash
                          </a>
                        </div>
                        <div role="toolbar" className="more">
                          <div className="btn-group">
                            <button
                              aria-expanded="false"
                              data-toggle="dropdown"
                              className="btn btn-dark dropdown-toggle"
                              type="button"
                            >
                              More
                              <span className="caret m-l-5" />
                            </button>
                            <div className="dropdown-menu">
                              <span className="dropdown-header">More Option :</span>{" "}
                              <a href="javascript: void(0);" className="dropdown-item">
                                Mark as Unread
                              </a>{" "}
                              <a href="javascript: void(0);" className="dropdown-item">
                                Add to Tasks
                              </a>{" "}
                              <a href="javascript: void(0);" className="dropdown-item">
                                Add Star
                              </a>
                              <a href="javascript: void(0);" className="dropdown-item">
                                Mute
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="email-list m-t-15">
                        <div className="message">
                          <a href="#myModal2" className="trigger-btn" data-toggle="modal">
                            <div className="col-mail col-mail-1">
                              <div className="email-checkbox">
                                <input type="checkbox" id="chk2" />
                                <label className="toggle" htmlFor="chk2" />
                              </div>
                              <span className="star-toggle ti-star" />
                            </div>
                            <div className="col-mail col-mail-2">
                              <div className="subject">Call Didn't Connect</div>
                              <div className="date">11:49 am</div>
                            </div>
                          </a>
                          <div id="myModal2" className="modal fade">
                            <div className="modal-dialog modal-login">
                              <div className="card">
                                <div className="card-body">
                                  <div className="email-center-box">
                                    <a
                                      href="email-compose.html"
                                      className="btn btn-primary btn-block"
                                    >
                                      Compose
                                    </a>
                                    <div className="mail-list mt-4">
                                      <a
                                        href="email-inbox.html"
                                        className="list-group-item border-0 text-primary p-r-0"
                                      >
                                        <i className="fa fa-inbox font-18 align-middle mr-2" />{" "}
                                        <b>Inbox</b>{" "}
                                        <span className="badge badge-primary badge-sm float-right m-t-5">
                                          198
                                        </span>{" "}
                                      </a>
                                      <a
                                        href="#"
                                        className="list-group-item border-0 p-r-0"
                                      >
                                        <i className="fa fa-paper-plane font-18 align-middle mr-2" />
                                        Sent
                                      </a>{" "}
                                      <a
                                        href="#"
                                        className="list-group-item border-0 p-r-0"
                                      >
                                        <i className="fa fa-star-o font-18 align-middle mr-2" />
                                        Important{" "}
                                        <span className="badge badge-danger badge-sm float-right m-t-5">
                                          47
                                        </span>{" "}
                                      </a>
                                      <a
                                        href="#"
                                        className="list-group-item border-0 p-r-0"
                                      >
                                        <i className="mdi mdi-file-document-box font-18 align-middle mr-2" />
                                        Draft
                                      </a>
                                      <a
                                        href="#"
                                        className="list-group-item border-0 p-r-0"
                                      >
                                        <i className="fa fa-trash font-18 align-middle mr-2" />
                                        Trash
                                      </a>
                                    </div>
                                    <h5 className="mt-5 m-b-10">Categories</h5>
                                    <div className="list-group mail-list">
                                      <a href="#" className="list-group-item border-0">
                                        <span className="fa fa-briefcase f-s-14 mr-2" />
                                        Work
                                      </a>{" "}
                                      <a href="#" className="list-group-item border-0">
                                        <span className="fa fa-sellsy f-s-14 mr-2" />
                                        Private
                                      </a>{" "}
                                      <a href="#" className="list-group-item border-0">
                                        <span className="fa fa-ticket f-s-14 mr-2" />
                                        Support
                                      </a>{" "}
                                      <a href="#" className="list-group-item border-0">
                                        <span className="fa fa-tags f-s-14 mr-2" />
                                        Social
                                      </a>
                                    </div>
                                  </div>
                                  <div className="email-right-box">
                                    <div className="toolbar" role="toolbar">
                                      <div className="btn-group m-b-20">
                                        <button type="button" className="btn btn-light">
                                          <i className="fa fa-archive" />
                                        </button>
                                        <button type="button" className="btn btn-light">
                                          <i className="fa fa-exclamation-circle" />
                                        </button>
                                        <button type="button" className="btn btn-light">
                                          <i className="fa fa-trash" />
                                        </button>
                                      </div>
                                      <div className="btn-group m-b-20">
                                        <button
                                          type="button"
                                          className="btn btn-light dropdown-toggle"
                                          data-toggle="dropdown"
                                        >
                                          <i className="fa fa-folder" />{" "}
                                          <b className="caret m-l-5" />
                                        </button>
                                        <div className="dropdown-menu">
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Social
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Promotions
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Updates
                                          </a>
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Forums
                                          </a>
                                        </div>
                                      </div>
                                      <div className="btn-group m-b-20">
                                        <button
                                          type="button"
                                          className="btn btn-light dropdown-toggle"
                                          data-toggle="dropdown"
                                        >
                                          <i className="fa fa-tag" />{" "}
                                          <b className="caret m-l-5" />
                                        </button>
                                        <div className="dropdown-menu">
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Updates
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Promotions
                                          </a>
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Forums
                                          </a>
                                        </div>
                                      </div>
                                      <div className="btn-group m-b-20">
                                        <button
                                          type="button"
                                          className="btn btn-light dropdown-toggle"
                                          data-toggle="dropdown"
                                        >
                                          More <span className="caret m-l-5" />
                                        </button>
                                        <div className="dropdown-menu">
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Mark as Unread
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Add to Tasks
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Add Star
                                          </a>{" "}
                                          <a
                                            className="dropdown-item"
                                            href="javascript: void(0);"
                                          >
                                            Mute
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="read-content">
                                      <div className="media pt-5">
                                        <img
                                          className="mr-3 rounded-circle"
                                          src="images/avatar/1.jpg"
                                        />
                                        <div className="media-body">
                                          <h5 className="m-b-3">Ingredia Nutrisha</h5>
                                          <p className="m-b-2">20 May 2018</p>
                                        </div>
                                      </div>
                                      <hr />
                                      <div className="media mb-4 mt-1">
                                        <div className="media-body">
                                          <span className="float-right">07:23 AM</span>
                                          <h4 className="m-0 text-primary">
                                            A collection of textile samples lay spread
                                          </h4>
                                          <small className="text-muted">
                                            To:Me,invernessmckenzie@example.com
                                          </small>
                                        </div>
                                      </div>
                                      <h5 className="m-b-15">Hi,Ingredia,</h5>
                                      <p>
                                        <strong>Ingredia Nutrisha,</strong> A collection
                                        of textile samples lay spread out on the table -
                                        Samsa was a travelling salesman - and above it
                                        there hung a picture
                                      </p>
                                      <p>
                                        Even the all-powerful Pointing has no control
                                        about the blind texts it is an almost
                                        unorthographic life One day however a small line
                                        of blind text by the name of Lorem Ipsum decided
                                        to leave for the far World of Grammar.
                                      </p>
                                      <p>
                                        Aenean vulputate eleifend tellus. Aenean leo
                                        ligula, porttitor eu, consequat vitae, eleifend
                                        ac, enim. Aliquam lorem ante, dapibus in, viverra
                                        quis, feugiat a, tellus. Phasellus viverra nulla
                                        ut metus varius laoreet. Quisque rutrum. Aenean
                                        imperdiet. Etiam ultricies nisi vel augue.
                                        Curabitur ullamcorper ultricies nisi. Nam eget
                                        dui. Etiam rhoncus. Maecenas tempus, tellus eget
                                        condimentum rhoncus, sem quam semper libero, sit
                                        amet adipiscing sem neque sed ipsum. Nam quam
                                        nunc, blandit vel, luctus pulvinar,
                                      </p>
                                      <h5 className="m-b-5 p-t-15">Kind Regards</h5>
                                      <p>Mr Smith</p>
                                      <hr />
                                      <h6 className="p-t-15">
                                        <i className="fa fa-download mb-2" /> Attachments{" "}
                                        <span>(3)</span>
                                      </h6>
                                      <div className="row m-b-30">
                                        <div className="col-auto">
                                          <a href="#" className="text-muted">
                                            My-Photo.png
                                          </a>
                                        </div>
                                        <div className="col-auto">
                                          <a href="#" className="text-muted">
                                            My-File.docx
                                          </a>
                                        </div>
                                        <div className="col-auto">
                                          <a href="#" className="text-muted">
                                            My-Resume.pdf
                                          </a>
                                        </div>
                                      </div>
                                      <hr />
                                      <div className="form-group p-t-15">
                                        <textarea
                                          className="w-100 p-20 l-border-1"
                                          name
                                          id
                                          cols={30}
                                          rows={5}
                                          placeholder="It's really an amazing.I want to know more about it..!"
                                          defaultValue={""}
                                        />
                                      </div>
                                    </div>
                                    <div className="text-right">
                                      <button
                                        className="btn btn-primaryw-md m-b-30"
                                        type="button"
                                      >
                                        Send
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="message">
                          <a href="#myModal2" className="trigger-btn" data-toggle="modal">
                            <div className="col-mail col-mail-1">
                              <div className="email-checkbox">
                                <input type="checkbox" id="chk3" />
                                <label className="toggle" htmlFor="chk3" />
                              </div>
                              <span className="star-toggle ti-star" />
                            </div>
                            <div className="col-mail col-mail-2">
                              <div className="subject">Appointment Rescheduled</div>
                              <div className="date">11:49 am</div>
                            </div>
                          </a>
                        </div>
                        <div className="message">
                          <a href="#myModal2" className="trigger-btn" data-toggle="modal">
                            <div className="col-mail col-mail-1">
                              <div className="email-checkbox">
                                <input type="checkbox" id="chk4" />
                                <label className="toggle" htmlFor="chk4" />
                              </div>
                              <span className="star-toggle ti-star" />
                            </div>
                            <div className="col-mail col-mail-2">
                              <div className="subject">Call Back Later</div>
                              <div className="date">11:49 am</div>
                            </div>
                          </a>
                        </div>
                      </div>
                      {/* panel */}
                      <div className="row">
                        <div className="col-7">
                          <div className="text-left">1 - 20 of 568</div>
                        </div>
                        <div className="col-5">
                          <div className="btn-group float-right">
                            <button className="btn btn-gradient" type="button">
                              <i className="fa fa-angle-left" />
                            </button>
                            <button className="btn btn-dark" type="button">
                              <i className="fa fa-angle-right" />
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="tab-pane fade" id="notestab">
                    <div className="col-md-12">
                      {/* <div className="notestab">
                        <h4>Plan &amp; History</h4>
                        <p>
                          <b>Plan: </b>{this.props.data.packageName}
                        </p>
                        <p>
                          <b>Corporate:</b> {this.props.data.employer}
                        </p>
                        <p>
                          <b>Facility: </b>{this.props.data.facilityName}
                        </p>
                      </div> */}
                      <div className="accordion" id="accordionExample2">
                        <ul className="timeline">
                        {
                        this.state.tasknotes && this.state.tasknotes.length ? this.state.tasknotes.map((e, key) => {  
                                  // return <option key={key} value={e.facility_id}>{e.address}</option>;  
                  return (
                          <li className="card">
                            <button
                              className="btn btn-link"
                              type="button"
                              data-toggle="collapse"
                              data-target="#collapseOne1"
                              aria-expanded="true"
                              aria-controls="collapseOne1"
                            >
                              {e.noteTitle}
                            </button>
                            <div
                              id="collapseOne1"
                              className="collapse"
                              aria-labelledby="headingOne"
                              data-parent="#accordionExample2"
                            >
                              <p>
                              {e.noteDesc}
                              </p>
                            </div>
                          </li>
                );
            })
            :<div className="center">No notes available</div>
            }  

                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className="tab-pane fade" id="lifecycle">
                    <div className="col-md-12">
                      <div className="lifecycle">
                        <h4>Plan &amp; History</h4>
                        <p>
                          <b>Plan: </b>{this.props.data.packageName}
                        </p>
                        <p>
                          <b>Corporate:</b> {this.props.data.employer}
                        </p>
                        <p>
                          <b>Facility: </b>{this.props.data.facilityName}
                        </p>
                      </div>
                      <div className="accordion" id="accordionExample2">
                        <ul className="timeline">
                          <li className="card">
                            <button
                              className="btn btn-link"
                              type="button"
                              data-toggle="collapse"
                              data-target="#collapseOne1"
                              aria-expanded="true"
                              aria-controls="collapseOne1"
                            >
                              <b>Appointment Booked</b> - 19-05-2020 (10:00AM) 
                            </button>
                            <div
                              id="collapseOne1"
                              className="collapse"
                              aria-labelledby="headingOne"
                              data-parent="#accordionExample2"
                            >
                              <p>
                                <b>Rep/Admin:</b> Payal Sharma (25456787)
                              </p>
                              <p>
                                <b>Type:</b> Clinic Appointment
                              </p>
                              <p>
                                <b>Cycle:</b> 1
                              </p>
                              <p>
                                <b>Disposition:</b> Appointment Booked
                              </p>
                              <p>
                                <b>Scheduled date time:</b> 19-05-2020 10:00AM
                              </p>
                              <p>
                                <b>Doctor Name:</b> Dr gagandeep
                              </p>
                              <p>
                                <b>Consultation Type:</b>
                              </p>
                              <p>
                                <b>Remarks:</b> VC Booked for 19-04
                              </p>
                            </div>
                          </li>
                          <li className="card">
                            <button
                              className="btn btn-link collapsed totalcalls"
                              type="button"
                              data-toggle="collapse"
                              data-target="#collapseCall"
                              aria-expanded="false"
                              aria-controls="collapseCall"
                            >
                            <b>Call</b> - 12-05-2020 (02:00PM)
                            </button>
                            <div
                              id="collapseCall"
                              className="collapse"
                              aria-labelledby="headingOne"
                              data-parent="#accordionExample2"
                            >
                              <p>
                                <b>Team:</b> CC Team
                              </p>
                              <p>
                                <audio controls>
                                  <source src="./audio/beat.mp3" type="audio/mpeg" />
                                </audio>
                              </p>
                              <p>
                                <b>Rep/Admin:</b> Neetu Singh (25456787)
                              </p>
                              <p>
                                <b>Type:</b> Outbound, Manual Dial
                              </p>
                              <p>
                                <b>Time:</b> 05/13/2020 18:27:50
                              </p>
                            </div>
                          </li>
                          <li className="card">
                            <button
                              className="btn btn-link collapsed"
                              type="button"
                              data-toggle="collapse"
                              data-target="#collapseTwo2"
                              aria-expanded="false"
                              aria-controls="collapseTwo2"
                            >
                              <b>Appointment Booked</b> - 15-04-2020 (10:00AM)
                            </button>
                            <div
                              id="collapseTwo2"
                              className="collapse"
                              aria-labelledby="headingOne"
                              data-parent="#accordionExample2"
                            >
                              <p>
                                <b>Rep/Admin:</b> Neetu Singh (25456787)
                              </p>
                              <p>
                                <b>Type:</b> Video Consultation
                              </p>
                              <p>
                                <b>Cycle:</b> 1
                              </p>
                              <p>
                                <b>Disposition:</b> Appointment Booked
                              </p>
                              <p>
                                <b>Scheduled date time:</b> 19-04-2020 10:00AM
                              </p>
                              <p>
                                <b>Doctor Name:</b> Dr gagandeep
                              </p>
                              <p>
                                <b>Consultation Type:</b>
                              </p>
                              <p>
                                <b>Remarks:</b> VC Booked for 19-04-2020
                              </p>
                            </div>
                          </li>
                          <li className="card">
                            <button
                              className="btn btn-link collapsed"
                              type="button"
                              data-toggle="collapse"
                              data-target="#collapseThree3"
                              aria-expanded="false"
                              aria-controls="collapseThree3"
                            >
                              <b>Appointment Booked</b> - 11-03-2020 (10:00AM)
                            </button>
                            <div
                              id="collapseThree3"
                              className="collapse"
                              aria-labelledby="headingOne"
                              data-parent="#accordionExample2"
                            >
                              <p>
                                <b>Rep/Admin:</b> Rupali Dutta (25456787)
                              </p>
                              <p>
                                <b>Type:</b> Audio Consultation
                              </p>
                              <p>
                                <b>Cycle:</b> 1
                              </p>
                              <p>
                                <b>Disposition:</b> Appointment Booked
                              </p>
                              <p>
                                <b>Scheduled date time:</b> 16-03-2020 10:00AM
                              </p>
                              <p>
                                <b>Doctor Name:</b> Dr gagandeep
                              </p>
                              <p>
                                <b>Consultation Type:</b>
                              </p>
                              <p>
                                <b>Remarks:</b> VC Booked for 16-03-2020
                              </p>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="profile2">
                    <div className="col-md-12">
                      <div className="row">
                        <div className="col-md-3 profileicons">
                          <i className="fa fa-user-circle-o" />
                        </div>
                        <div className="col-md-9 profiledetails">
                          <h4>{this.props.data.projectName}</h4>
                          <p>
                            <strong>PAN: </strong> {this.props.data.projectIdentifier}{" "}
                          </p>
                          <p>
                            <strong>Employee Id: </strong> {this.props.data.employeeId}{" "}
                          </p>
                          <p>
                            <strong>Employer: </strong> {this.props.data.employer}{" "}
                          </p>
                          
                          <p>
                            <strong>Email: </strong> {this.props.data.emailID}{" "}
                          </p>
                          <p>
                            <strong>Mobile: </strong> {this.props.data.patientMobile}{" "}
                          </p>
                          <p>
                            <strong>Age: </strong> {this.props.data.age}{" "}
                          </p>
                          <p>
                            <strong>Gender: </strong> {this.props.data.gender}{" "}
                          </p>
                          <p>
                            <strong>Bill Number: </strong> {this.props.data.billNumber}{" "}
                          </p>
                          <p>
                            <strong>Facility Name: </strong> {this.props.data.facilityName}{" "}
                          </p>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="chat">
                    <div className="col-md-12 smschat">
                      <h4>{this.props.data.projectName} - SMS</h4>
                      <ul className="sms">
                      {this.state.pageOfItems && this.state.pageOfItems.length ? this.state.pageOfItems.map((e, key) => {  
                                  // return <option key={key} value={e.facility_id}>{e.address}</option>;  
                  return (
                        <li className="left clearfix">
                          {/* <span className="chat-img pull-left">
                          </span> */}
                          <div className="chat-body clearfix">
                            <div>
                              <strong className="primary-font">
                              {e.destination_mobile_no}
                              </strong>{" "}
                              <small className="pull-right text-muted">
                                <span className="glyphicon glyphicon-time" />
                                {/* created {this.daysDiff(new Date(e.sms_creation_date),new Date()) } days ago */}
                                {this.formatDate(new Date(e.sms_creation_date))}
                              </small>
                            
                            </div>
                            <p >

                              {e.sms_body}
                            </p>
                            <small className="primary-font">
                              Sent By - <b>{this.props.data.tenantName}</b>
                              </small>{" "}
                          </div>
                        </li>
                        );
                      })
                    :''
                    }  

                        {/* <li className="left clearfix">
                          <span className="chat-img pull-left">
                        
                          </span>
                          <div className="chat-body clearfix">
                            <div>
                              <strong className="primary-font">
                                Sanchit Singh Khare
                              </strong>{" "}
                              <small className="pull-right text-muted">
                                <span className="glyphicon glyphicon-time" />1 days ago
                              </small>
                            </div>
                            <p>
                              Booking Reservation SMS Notification for Video Consultation:
                              Using this module, the store admin can send SMS
                              notifications to customers for booking. A customer can
                              receive mobile text messages for - booking created,
                              cancelled, reminder alerts. The admin can also send bulk SMS
                              to customers.
                            </p>
                          </div>
                        </li> */}
                        {/* <li className="left clearfix">
                          <span className="chat-img pull-left">
                      
                          </span>
                          <div className="chat-body clearfix">
                            <div>
                              <strong className="primary-font">
                                Sanchit Singh Khare
                              </strong>{" "}
                              <small className="pull-right text-muted">
                                <span className="glyphicon glyphicon-time" />
                                12 mins ago
                              </small>
                            </div>
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                              Curabitur bibendum ornare dolor, quis ullamcorper ligula
                              sodales.
                            </p>
                          </div>
                        </li> */}
                      </ul>
                      <div className="input-group">
                        <textarea
                          id="textsmsbody"
                          className="form-control input-default"
                          placeholder="Type your message here..."
                          rows="4" cols="50"
                          name="sms_body" 
                              value={this.state.sms_body}
                              onChange={this.onChange} 
                        />
                      </div>
                      <span className="input-group-btn">
                          <button
                            className="btn btn-primary form-control input-default"
                            id="btn-chat"  onClick={this.onClick}
                          >
                            Send
                          </button>
                          <div className="card">
                          <PaginationSMS
            items={this.props.smsdata} 
            onChangePage={this.onChangePage} />
            </div>
                        </span>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="videoCall">
                    <div className="videocallbuttons">
                      <div className="lnk">
                        <button className="btn bounceInUp animated">
                          <i className="fa fa fa-microphone" />
                        </button>
                      </div>
                      <div className="lnk">
                        <button className="btn bounceInUp animated">
                          <i className="fa fa-volume-up" />
                        </button>
                      </div>
                      <div className="lnk">
                        <button className="btn bounceInUp animated">
                          <i className="fa fa-camera" />
                        </button>
                      </div>
                      <div className="lnk">
                        <button className="btn bounceInUp animated">
                          <i className="fa fa-video-camera" />
                        </button>
                      </div>
                      <div className="lnk">
                        <button className="btn call-end bounceInUp animated">
                          <i className="fa fa-phone" />
                        </button>
                      </div>
                    </div>
                    <video autoPlay="true" id="videoElement" />
                  </div>
                  <div className="tab-pane fade" id="portals">
                    {/* <h4>
                      <a
                        href="http://ec2-18-140-50-84.ap-southeast-1.compute.amazonaws.com:5000/"
                        target="_blank"
                      >
                        CW
                      </a>
                    </h4> */}
                    {/* <h4>
                      <a
                        href="https://healthyhcl.in/ehc/selfappointment.php"
                        target="_blank"
                      >
                        EHC
                      </a>
                    </h4> */}
                    <h4>
                      <iframe className="framestyle" src="https://hcl-h-exit.web.app/" ></iframe>
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
            );
  }
}

// TaskPane.propTypes = {
//   deleteProject: PropTypes.func.isRequired
// };



TaskPane.propTypes = {
  getTask: PropTypes.func.isRequired,
  createTask: PropTypes.func.isRequired,
  deleteTaskwoConfirm:PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project.project,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getTask, createTask,deleteTaskwoConfirm }
)(TaskPane);
