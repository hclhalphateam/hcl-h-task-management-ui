import React, { Component } from "react";
import { getTask, createTask } from "../../actions/taskActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classnames from "classnames";
import { Link } from "react-router-dom";

class UpdateTask extends Component {
  //set state
  constructor() {
    super();

    this.state = {
      id: "",
      taskId:"",
      projectName: "",
      projectIdentifier: "",
      description: "",
      start_date: "",
      end_date: "",
      currentTaskStatus:"",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    const {
      id,
      taskId,
      projectName,
      projectIdentifier,
      description,
      start_date,
      end_date,
      currentTaskStatus
    } = nextProps.project;

    this.setState({
      id,
      taskId,
      projectName,
      projectIdentifier,
      description,
      start_date,
      end_date,
      currentTaskStatus
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getTask(id, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const updateProject = {
      id: this.state.id,
      taskId: this.state.taskId,
      projectName: this.state.projectName,
      projectIdentifier: this.state.projectIdentifier,
      description: this.state.description,
      start_date: this.state.start_date,
      end_date: this.state.end_date,
      currentTaskStatus:this.state.currentTaskStatus
    };

    this.props.createTask(updateProject, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="project scroll"> 
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
            {/* <Link to={`/`} className="btn btn-dark">
                Back to MyTasks
              </Link> */}
              <h4 className="display-5 text-center">Update an Incident</h4>
              <hr />
              <form className="form-details scroll" onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control ", {
                      "is-invalid": errors.projectName
                    })}
                    placeholder="Task Name"
                    name="projectName"
                    value={this.state.projectName}
                    onChange={this.onChange}
                  />
                  {errors.projectName && (
                    <div className="invalid-feedback">{errors.projectName}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Pan ID"
                    name="projectIdentifier"
                    value={this.state.projectIdentifier}
                    onChange={this.onChange}
                    disabled
                  />
                </div>
                <div className="form-group">
                  <textarea
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.description
                    })}
                    placeholder="Event Description"
                    name="description"
                    onChange={this.onChange}
                    value={this.state.description}
                  />
                  {errors.description && (
                    <div className="invalid-feedback">{errors.description}</div>
                  )}
                </div>
                <h6>Start Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="start_date"
                    value={this.state.start_date}
                    onChange={this.onChange}
                  />
                </div>
                <h6>Estimated End Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg "
                    name="end_date"
                    value={this.state.end_date}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="currentTaskStatus"
                    value={this.state.currentTaskStatus}
                    onChange={this.onChange}
                  >
                    <option value="">Select Status</option>
                    <option value="Assigned">Assigned</option>
                    <option value="Completed">Completed</option>
                  </select>
                </div>

                <input
                  type="submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateTask.propTypes = {
  getTask: PropTypes.func.isRequired,
  createTask: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project.project,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getTask, createTask }
)(UpdateTask);
