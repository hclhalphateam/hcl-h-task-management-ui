const container_url= "https://dsp.hclheathcare.in"
const retries =10;
const sleep_time =3000;
export const INSTRUCTIONS = {
    REDIRECT:'REDIRECT',
    AUTH_SETTER:'AUTH_SETTER',
    AUTH_GETTER:'AUTH_GETTER',
    HEADER_COLLAPSE:'HEADER_COLLAPSE'
}
export const ACCESS={
    ADMIN:'ADMIN',
    CC:'CC',
    SUPERVISOR:'SUPERVISOR'
}
export const REDIRECT = {
    ADMIN:'ADMIN',
    CC:'CC',
    SUPERVISOR:'SUPERVISOR',
    LOGIN:'login'
}

export const containerService = (function () {

    // Instance stores a reference to the Singleton
    let instance;

    function init() {

        // Singleton
        let callback_id=0;
        let get_new_call_back_id= function(){
            callback_id+=1; return callback_id
        }
        let listener = messageListener.getListener()
        let sleep=(ms)=> new Promise(resolve => setTimeout(resolve, ms));

        return {

            // Public methods and variables
            synchronizedSend: async(message)=>{

                message.callback_id=get_new_call_back_id()
                window.parent.postMessage(message, container_url)
                let res=null;

                for (let i = 1;  i<retries; ++i){

                    res= listener.getRes(message.callback_id)
                    if(res!==undefined && res !==null) break;
                    await sleep(sleep_time);

                }
                console.log(res)
                return res

            },
            send: (data)=>{
                window.parent.postMessage(data, container_url)
            },
            queue:listener.queue

            // publicProperty: "I am also public"
        };

    };

    return {

        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {

            if ( !instance ) {
                instance = init();
            }

            return instance;
        }

    };

})();

const  messageListener = (function(){

    let res = new Map();
    let queue=[];



    (function(){
        window.addEventListener("message",
            (event)=>{
                console.log("%c listener message: ", "font-size:20px; color:pink")
                console.log(event.data)
                if(event.origin===container_url) //security check
                {
                    if(event.data.callback_id!==undefined)
                        res[event.data.callback_id]=event.data // puts data in the Map
                    else
                        queue.push(event.data)


                }

                //else no callback_id
                console.log()
                console.log(res)

            })
    })();

    function init() {

        return {
            // Public methods and variables
            getRes: (callback_id)=>{return res[callback_id]},
            res:()=>res,
            queue:queue

        };

    };

    return {
        getListener: ()=>init()
    }

})();