import axios from 'axios'
import {auth} from "../auth/index";


let authInstance = auth.getInstance();
axios.interceptors.request.use((config) => {
// Do something before request is sent
console.log(authInstance)
    config.headers.Authorization=authInstance.getToken()
    return config;
}, (error) => {
    // Do something with request error
return Promise.reject(error);
});
// Add a response interceptor
axios.interceptors.response.use((response) => {
    // Do something with response data
return response;
}, (error) => {
    // Do something with response error
return Promise.reject(error);
});
