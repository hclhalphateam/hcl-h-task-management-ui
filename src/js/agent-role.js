
window.$(function () {
        DialPad.init();
      });
      var DialPad = {
        init: function () {
          this.filter();
          this.keypad();
          this.dialing();
        },
        dialing: function () {
          window.$('.people').click(function () {
            DialPad.call(window.$(this));
          });
          window.$('.action .btn').click(function () {
            window.$(this).toggleClass('active');
          });
          window.$('.call-end').click(function () {
            window.$('.left-pan').removeClass('active');
            window.$('.calling').fadeOut(100);
            window.$('.contacts').fadeIn(800);
            window.$('.calling .photo').html('');
            window.$('.calling .name').html('Unknown');
            window.$('.calling .number').html('');
          });
        },
        keypad: function () {
          window.$('.dial-key-wrap').click(function () {
            var key = window.$(this).data('key');
            var display = window.$('.dial-screen').text();
            switch (key) {
              case 'back':
                DialPad.press(window.$('.dial-key-wrap[data-key="back"]'));
                display = window.$('.dial-screen').text(display.substring(0, display.length - 1));
                // DialPad.longClick();
                break;
              case 'call':
                DialPad.press(window.$('.dial-key-wrap[data-key="call"]'));
                DialPad.call();
                break;
              case 0:
                DialPad.press(window.$('.dial-key-wrap[data-key="0"]'));
                display = window.$('.dial-screen').text(display + '0');
                break;
              case 1:
                DialPad.press(window.$('.dial-key-wrap[data-key="1"]'));
                display = window.$('.dial-screen').text(display + '1');
                break;
              case 2:
                DialPad.press(window.$('.dial-key-wrap[data-key="2"]'));
                display = window.$('.dial-screen').text(display + '2');
                break;
              case 3:
                DialPad.press(window.$('.dial-key-wrap[data-key="3"]'));
                display = window.$('.dial-screen').text(display + '3');
                break;
              case 4:
                DialPad.press(window.$('.dial-key-wrap[data-key="4"]'));
                display = window.$('.dial-screen').text(display + '4');
                break;
              case 5:
                DialPad.press(window.$('.dial-key-wrap[data-key="5"]'));
                display = window.$('.dial-screen').text(display + '5');
                break;
              case 6:
                DialPad.press(window.$('.dial-key-wrap[data-key="6"]'));
                display = window.$('.dial-screen').text(display + '6');
                break;
              case 7:
                DialPad.press(window.$('.dial-key-wrap[data-key="7"]'));
                display = window.$('.dial-screen').text(display + '7');
                break;
              case 8:
                DialPad.press(window.$('.dial-key-wrap[data-key="8"]'));
                display = window.$('.dial-screen').text(display + '8');
                break;
              case 9:
                DialPad.press(window.$('.dial-key-wrap[data-key="9"]'));
                display = window.$('.dial-screen').text(display + '9');
                break;
              case '*':
                DialPad.press(window.$('.dial-key-wrap[data-key="*"]'));
                display = window.$('.dial-screen').text(display + '*');
                break;
              case '#':
                DialPad.press(window.$('.dial-key-wrap[data-key="#"]'));
                display = window.$('.dial-screen').html(display + '#');
                break;}

            DialPad.filter();
          });
          window.$(document).keydown(function (e) {
            var key = e.which;
            var screen = window.$('.dial-screen').text();

            switch (e.which) {
              case 8:
                DialPad.press(window.$('.dial-key-wrap[data-key="back"]'));
                screen = window.$('.dial-screen').text(screen.substring(0, screen.length - 1));
                break;
              case 13:
                DialPad.press(window.$('.dial-key-wrap[data-key="call"]'));
                DialPad.call();
                break;
              case 96:
                DialPad.press(window.$('.dial-key-wrap[data-key="0"]'));
                screen = window.$('.dial-screen').text(screen + '0');
                break;
              case 97:
                DialPad.press(window.$('.dial-key-wrap[data-key="1"]'));
                screen = window.$('.dial-screen').text(screen + '1');
                break;
              case 98:
                DialPad.press(window.$('.dial-key-wrap[data-key="2"]'));
                screen = window.$('.dial-screen').text(screen + '2');
                break;
              case 99:
                DialPad.press(window.$('.dial-key-wrap[data-key="3"]'));
                screen = window.$('.dial-screen').text(screen + '3');
                break;
              case 100:
                DialPad.press(window.$('.dial-key-wrap[data-key="4"]'));
                screen = window.$('.dial-screen').text(screen + '4');
                break;
              case 101:
                DialPad.press(window.$('.dial-key-wrap[data-key="5"]'));
                screen = window.$('.dial-screen').text(screen + '5');
                break;
              case 102:
                DialPad.press(window.$('.dial-key-wrap[data-key="6"]'));
                screen = window.$('.dial-screen').text(screen + '6');
                break;
              case 103:
                DialPad.press(window.$('.dial-key-wrap[data-key="7"]'));
                screen = window.$('.dial-screen').text(screen + '7');
                break;
              case 104:
                DialPad.press(window.$('.dial-key-wrap[data-key="8"]'));
                screen = window.$('.dial-screen').text(screen + '8');
                break;
              case 105:
                DialPad.press(window.$('.dial-key-wrap[data-key="9"]'));
                screen = window.$('.dial-screen').text(screen + '9');
                break;}

            var array = [8, 13, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
            var prevent = true;
            for (var i = 0; i < array.length; i++) {if (window.CP.shouldStopExecution(0)) break;
              if (key == array[i]) {
                DialPad.filter();
                break;
              }
            }window.CP.exitedLoop(0);
            if (key == 8) {
              return false;
            }
          });
        },
        call: function (info) {
          var num = window.$('.dial-screen').text().length;
          if (num > 0) {
            var photo, name, number;
            window.$('.left-pan').addClass('active');
            window.$('.contacts').fadeOut(100);
            window.$('.calling').fadeIn(800);
            if (info) {
              photo = info.find('img').attr('src') ? '<img src="' + info.find('img').attr('src') + '">' : null;
              name = info.find('.name').text() ? info.find('.name').text() : 'Unknown';
              number = info.find('.phone').text();
              window.$('.calling .photo').html(photo);
              window.$('.calling .name').text(name);
              window.$('.calling .number').text(number);
            } else {
              window.$('.calling .number').text(window.$('.dial-screen').text());
            }
            setTimeout(function () {
              window.$('.call-end .btn').focus();
            }, 800);
          }
          var num = window.$('.dial-screen1').text().length;
          if (num > 0) {
            var photo, name, number;
            window.$('.left-pan').addClass('active');
            window.$('.contacts').fadeOut(100);
            window.$('.calling').fadeIn(800);
            if (info) {
              photo = info.find('img').attr('src') ? '<img src="' + info.find('img').attr('src') + '">' : null;
              name = info.find('.name').text() ? info.find('.name').text() : 'Unknown';
              number = info.find('.phone').text();
              window.$('.calling .photo').html(photo);
              window.$('.calling .name').text(name);
              window.$('.calling .number').text(number);
            } else {
              window.$('.calling .number').text(window.$('.dial-screen').text());
            }
            setTimeout(function () {
              window.$('.call-end .btn').focus();
            }, 800);
          }
        },
        filter: function () {
          var highlight = function (string) {
            window.$('.people .number.match').each(function () {
              var matchStart = window.$(this).text().toLowerCase().indexOf('' + string.toLowerCase() + '');
              var matchEnd = matchStart + string.length - 1;
              var beforeMatch = window.$(this).text().slice(0, matchStart);
              var matchText = window.$(this).text().slice(matchStart, matchEnd + 1);
              var afterMatch = window.$(this).text().slice(matchEnd + 1);
              window.$(this).html(beforeMatch + '<span class="highlight">' + matchText + '</span>' + afterMatch);
            });
          };
          var showcontact = function () {
            window.$('.people .number').each(function () {
              if (window.$(this).css('display') == 'inline') {
                window.$(this).parent().parent().parent().show();
              } else {
                window.$(this).parent().parent().parent().hide();
              }
            });
          };
          //var refine = function(){
          var _this = window.$('.dial-screen');
          if (_this.text()) {
            window.$('.people .number').removeClass('match').hide().filter(function () {
              return window.$(this).text().toLowerCase().indexOf(_this.text().toLowerCase()) != -1;
            }).addClass('match').show();
            highlight(_this.text());
            window.$('.people').show();
            showcontact();
          } else {
            window.$('.people,.people .number').removeClass('match').hide();
          }
          //};
        },
        press: function (obj) {
          var button = obj.addClass('active');
          setTimeout(function () {
            button.removeClass('active');
          }, 200);
        } };

       window.$(document).ready(function(){
       
       var preloadbg = document.createElement("img");
       preloadbg.src = "https://s3-us-west-2.amazonaws.com/s.cdpn.io/245657/timeline1.png";
       
       window.$("#searchfield").focus(function(){
         if(window.$(this).val() == "Search contacts..."){
           window.$(this).val("");
         }
       });
       window.$("#searchfield").focusout(function(){
         if(window.$(this).val() == ""){
           window.$(this).val("Search contacts...");
         }
       });
       window.$("#sendmessage input").focus(function(){
         if(window.$(this).val() == "Send message..."){
           window.$(this).val("");
         }
       });
       window.$("#sendmessage input").focusout(function(){
         if(window.$(this).val() == ""){
           window.$(this).val("Send message..."); 
         }
       });
       window.$(".friend").each(function(){   
         window.$(this).click(function(){
           var childOffset = window.$(this).offset();
           var parentOffset = window.$(this).parent().parent().offset();
           var childTop = childOffset.top - parentOffset.top;
           var clone = window.$(this).find('img').eq(0).clone();
           var top = childTop+12+"px";
           
           window.$(clone).css({'top': top}).addClass("floatingImg").appendTo("#chatbox");                  
           
           setTimeout(function(){window.$("#profile p").addClass("animate");window.$("#profile").addClass("animate");}, 100);
           setTimeout(function(){
             window.$("#chat-messages").addClass("animate");
             window.$('.cx, .cy').addClass('s1');
             setTimeout(function(){window.$('.cx, .cy').addClass('s2');}, 100);
             setTimeout(function(){window.$('.cx, .cy').addClass('s3');}, 200);     
           }, 150);                            
           
           window.$('.floatingImg').animate({
             'width': "68px",
             'left':'108px',
             'top':'20px'
           }, 200);
           
           var name = window.$(this).find("p strong").html();
           var email = window.$(this).find("p span").html();                            
           window.$("#profile p").html(name);
           window.$("#profile span").html(email);     
           
           window.$(".message").not(".right").find("img").attr("src", window.$(clone).attr("src"));                  
           window.$('#friendslist').fadeOut();
           window.$('#chatview').fadeIn(); 
           window.$('#close').unbind("click").click(function(){       
             window.$("#chat-messages, #profile, #profile p").removeClass("animate");
             window.$('.cx, .cy').removeClass("s1 s2 s3");
             window.$('.floatingImg').animate({
               'width': "40px",
               'top':top,
               'left': '12px'
             }, 200, function(){window.$('.floatingImg').remove()});        
             
             setTimeout(function(){
               window.$('#chatview').fadeOut();
               window.$('#friendslist').fadeIn();       
             }, 50);
            });
          });
        });     
      });

      window.$(document).ready(function(){
        window.$("#select_doctors").click(function(){
          window.$(".doctorlist").toggle();
        });
        window.$(".advancesearch").click(function(){
          window.$(".filtersearch").show();
        });
        window.$(".advancesearch").click(function(){
          window.$(".advancesearch1").hide();
        });
        window.$(".normalsearch").click(function(){
          window.$(".advancesearch1").show();
        });
        window.$(".normalsearch").click(function(){
          window.$(".filtersearch").hide();
        });
      });



      