// Create IE + others compatible event handler

// attach postMessage event to handler

window.ameyo = window.ameyo || {};

ameyo.integration = (function() {
	var noOfChatsActive = 0;
	var chatIntegrationEnabled = true;
	var ameyoBaseUrl = "http://172.23.2.133:8888";
	var RECORD_INFO_FOR_SHOW_CRM = "recordInfoForShowCrm"
	var customFunctions = new Array();
	var callbacks = {};
	var methodId = 0;

	function processPostMessage(event) {
		var theObject = JSON.parse(event.data);
		if (theObject.method == 'showCrm') {
			var phone = theObject.phone;
			var requestId = theObject.requestId;
			var additionalParams = theObject.additionalParams;
			if (customFunctions['showCrm']) {
				customFunctions['showCrm'].showCrm(phone, additionalParams,
						requestId);
			}
		} else if (theObject.method == 'showCrmDetailed') {
			var recordInfo = theObject.recordInfo;
			if (customFunctions['showCrmDetailed']) {
				customFunctions['showCrmDetailed'].showCrmDetailed(recordInfo);
			}
		} else if (theObject.method == 'ifPortOpened') {
			var data = theObject.isPortOpened;
			if (customFunctions['ifPortOpened']) {
				customFunctions['ifPortOpened'].ifPortOpened(data);
			}
		} else if (theObject.method == 'selectExtensionData') {
			var data = JSON.parse(JSON.parse(theObject.detail.data).data);
			var selectedPhone = JSON.parse(theObject.detail.data).selectedPhone;
			if (customFunctions['selectExtensionData']) {
				customFunctions['selectExtensionData'].selectExtensionData(
						data, selectedPhone);
			}

		} else if (theObject.method == 'intializeUI') {
			try {
				var uiElementIds = theObject.uiElementIds;
				var disabledUiIds = configureUI(uiElementIds);
				hideUI(disabledUiIds);
			} catch (e) {
			}
		} else if (theObject.method == 'intializeLoginCredentials') {
			try {
				var loginCredential = getLoginInfo();
				setLoginInfo(loginCredential);
			} catch (e) {
			}
		} else if (theObject.method == 'intializeExtensionInfo') {
			try {
				var extensionInfo = getExtensionInfo();
				setExtensionInfo(extensionInfo);
			} catch (e) {
			}
		} else if (theObject.method == 'logoutHandler') {
			var reason = theObject.reason;
			if (customFunctions['logoutHandler']) {
				customFunctions['logoutHandler'].logoutHandler(reason);
			}
		} else if (theObject.method == 'loginHandler') {
			var reason = theObject.reason;
			if (customFunctions['loginHandler']) {
				customFunctions['loginHandler'].loginHandler(reason);
			}
		} else if (theObject.method == 'onLoadHandler') {
			if (customFunctions['onLoadHandler']) {
				customFunctions['onLoadHandler'].onLoadHandler();
			}
		} else if (theObject.method == 'loginStatusHandler') {
			var reason = theObject.reason;
			if (customFunctions['loginStatusHandler']) {
				customFunctions['loginStatusHandler']
						.loginStatusHandler(reason);
			}
		} else if (theObject.method == 'forceLoginHandler') {
			var reason = theObject.reason;
			if (customFunctions['forceLoginHandler']) {
				customFunctions['forceLoginHandler'].forceLoginHandler(reason);
			}
		} else if (theObject.method == 'selectExtensionHandler') {
			var reason = theObject.reason;
			if (customFunctions['selectExtensionHandler']) {
				customFunctions['selectExtensionHandler']
						.selectExtensionHandler(reason);
			}
		} else if (theObject.method == 'modifyExtensionHandler') {
			var reason = theObject.reason;
			if (customFunctions['modifyExtensionHandler']) {
				customFunctions['modifyExtensionHandler']
						.modifyExtensionHandler(reason);
			}
		} else if (theObject.method == 'selectCampaignHandler') {
			var reason = theObject.reason;
			if (customFunctions['selectCampaignHandler']) {
				customFunctions['selectCampaignHandler']
						.selectCampaignHandler(reason);
			}
		} else if (theObject.method == 'autoCallOnHandler') {
			var reason = theObject.reason;
			if (customFunctions['autoCallOnHandler']) {
				customFunctions['autoCallOnHandler'].autoCallOnHandler(reason);
			}
		} else if (theObject.method == 'autoCallOffHandler') {
			var reason = theObject.reason;
			if (customFunctions['autoCallOffHandler']) {
				customFunctions['autoCallOffHandler']
						.autoCallOffHandler(reason);
			}
		} else if (theObject.method == 'readyHandler') {
			var reason = theObject.reason;
			if (customFunctions['readyHandler']) {
				customFunctions['readyHandler'].readyHandler(reason);
			}
		} else if (theObject.method == 'breakHandler') {
			var reason = theObject.reason;
			if (customFunctions['breakHandler']) {
				customFunctions['breakHandler'].breakHandler(reason);
			}
		} else if (theObject.method == 'hangupHandler') {
			var reason = theObject.reason;
			if (customFunctions['hangupHandler']) {
				customFunctions['hangupHandler'].hangupHandler(reason);
			}
		} else if (theObject.method == 'handleUserCrtUpdatedHandler') {
			var reason = theObject.reason;
			if (customFunctions['handleUserCrtUpdatedHandler']) {
				customFunctions['handleUserCrtUpdatedHandler']
						.handleUserCrtUpdatedHandler(reason);
			}
		} else if (theObject.method == 'disposeCall') {
			if (customFunctions['disposeCall']) {
				customFunctions['disposeCall'].disposeCall();
			}
		} else if (theObject.method == 'chatDisposed') {
			if (noOfChatsActive > 0) {
				noOfChatsActive = noOfChatsActive - 1;
			}
			resizeChatIFrameToFitContent(noOfChatsActive);

			var agentCRTId = theObject.agentCRTId;
			if (customFunctions['chatDisposed']) {
				customFunctions['chatDisposed'].chatDisposed(agentCRTId);
			}
		} else if (theObject.method == 'chatInitiated') {
			if (theObject.customerId == null) {
				theObject.customerId = -1;
			}
			postMessageToChatIframe(theObject, ameyoBaseUrl);
			noOfChatsActive = noOfChatsActive + 1;
			resizeChatIFrameToFitContent(noOfChatsActive);
			var objectToSend = {};
			objectToSend.agentCRTId = theObject.agentCRTId;
			objectToSend.sessionId = theObject.sessionId;
			objectToSend.customerCRTId = theObject.customerCRTId;
			objectToSend.campaignId = theObject.campaignId;
			objectToSend.customerId = theObject.customerId;
			objectToSend.name = theObject.customerData.name;
			objectToSend.phone = theObject.customerData.phone;
			objectToSend.email = theObject.customerData.email;
			objectToSend.chatId = theObject.chatId;
			if (customFunctions['chatInitiated']) {
				customFunctions['chatInitiated'].chatInitiated(
						theObject.agentCRTId, theObject.customerCRTId,
						theObject.campaignId, theObject.customerId,
						theObject.customerData.name, theObject.chatId);
			}
		} else if (theObject.method == 'chatTransferredOrConferred') {
			if (theObject.customerId == null) {
				theObject.customerId = -1;
			}
			postMessageToChatIframe(theObject, ameyoBaseUrl);
			noOfChatsActive = noOfChatsActive + 1;
			resizeChatIFrameToFitContent(noOfChatsActive);
			var objectToSend = {};
			objectToSend.agentCRTId = theObject.agentCRTId;
			objectToSend.sessionId = theObject.sessionId;
			objectToSend.customerCRTId = theObject.customerCRTId;
			objectToSend.campaignId = theObject.campaignId;
			objectToSend.customerId = theObject.customerId;
			objectToSend.name = theObject.customerData.name;
			objectToSend.phone = theObject.customerData.phone;
			objectToSend.email = theObject.customerData.email;
			objectToSend.chatId = theObject.chatId;
			if (customFunctions['chatTransferredOrConferred']) {
				customFunctions['chatTransferredOrConferred']
						.chatTransferredOrConferred(theObject.agentCRTId
								.toString(),
								theObject.customerCRTId.toString(),
								theObject.campaignId.toString(),
								theObject.customerName.toString(),
								theObject.chatId.toString());
			}
		} else if (theObject.method == 'chatEnded') {
			var agentCRTId = theObject.agentCRTId;
			if (customFunctions['chatEnded']) {
				customFunctions['chatEnded'].chatEnded(agentCRTId);
			}
		} else if (theObject.method == 'transferToPhoneHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferToPhoneHandler']) {
				customFunctions['transferToPhoneHandler']
						.transferToPhoneHandler(reason);
			}
		} else if (theObject.method == 'transferInCallHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferInCallHandler']) {
				customFunctions['transferInCallHandler']
						.transferInCallHandler(reason);
			}
		} else if (theObject.method == 'transferToAQHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferToAQHandler']) {
				customFunctions['transferToAQHandler']
						.transferToAQHandler(reason);
			}
		} else if (theObject.method == 'transferToIVRHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferToIVRHandler']) {
				customFunctions['transferToIVRHandler']
						.transferToIVRHandler(reason);
			}
		} else if (theObject.method == 'transferToUserHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferToUserHandler']) {
				customFunctions['transferToUserHandler']
						.transferToUserHandler(reason);
			}
		} else if (theObject.method == 'transferToCampaignHandler') {
			var reason = theObject.reason;
			if (customFunctions['transferToCampaignHandler']) {
				customFunctions['transferToCampaignHandler']
						.transferToCampaignHandler(reason);
			}
		} else if (theObject.method == 'conferWithPhoneHandler') {
			var reason = theObject.reason;
			if (customFunctions['conferWithPhoneHandler']) {
				customFunctions['conferWithPhoneHandler']
						.conferWithPhoneHandler(reason);
			}
		} else if (theObject.method == 'conferWithTPVHandler') {
			var reason = theObject.reason;
			if (customFunctions['conferWithTPVHandler']) {
				customFunctions['conferWithTPVHandler']
						.conferWithTPVHandler(reason);
			}
		} else if (theObject.method == 'conferWithUserHandler') {
			var reason = theObject.reason;
			if (customFunctions['conferWithUserHandler']) {
				customFunctions['conferWithUserHandler']
						.conferWithUserHandler(reason);
			}
		} else if (theObject.method == 'conferWithLocalIVRHandler') {
			var reason = theObject.reason;
			if (customFunctions['conferWithLocalIVRHandler']) {
				customFunctions['conferWithLocalIVRHandler']
						.conferWithLocalIVRHandler(reason);
			}
		} else if (theObject.method == 'getDispositionCodesHandler') {
			var reason = theObject.dispositionCodes;
			if (customFunctions['getDispositionCodesHandler']) {
				customFunctions['getDispositionCodesHandler']
						.getDispositionCodesHandler(reason);
			}
		} else if (theObject.method == 'disposeCallByDispositionCodeHandler') {
			var reason = theObject.reason;
			if (customFunctions['disposeCallByDispositionCodeHandler']) {
				customFunctions['disposeCallByDispositionCodeHandler']
						.disposeCallByDispositionCodeHandler(reason);
			}
		} else if (theObject.method == 'disposeAndDialHandler') {
			var reason = theObject.reason;
			if (customFunctions['disposeAndDialHandler']) {
				customFunctions['disposeAndDialHandler']
						.handleDisposeAndDial(reason);
			}
		} else if (theObject.method == 'initializeChat') {
			postMessageToChatIframe(theObject, ameyoBaseUrl);
		} else if (theObject.method == 'closeChatWindow') {
			postMessageToChatIframe(theObject, ameyoBaseUrl);
		} else if (theObject.method == 'getDispositions') {
			onDispositionsFetched(theObject.data);
		} else if (theObject.method == 'highlightCrm') {
			refreshTelephony(theObject.agentCRT);
			if (customFunctions['highlightCrm']) {
				customFunctions['highlightCrm']
						.highlightCrm(theObject.agentCRT);
			}
		} else {
			var methodName = theObject.method;
			if (callbacks.hasOwnProperty(methodName)) {
				for ( var i in callbacks[methodName]) {
					callbacks[methodName][i](theObject);
				}
				delete callbacks[methodName];
			}
		}
	}
	function addIframeForChatWindow() {
		ifrm = document.createElement("IFRAME");
		ifrm.setAttribute("src", ameyoBaseUrl
				+ "/ameyochatjs/embeddedUserChat-default.html");
		ifrm.setAttribute("name", "userChatIframe");
		ifrm.setAttribute("id", "userChatIframe");
		ifrm.setAttribute("marginheight", "1");
		ifrm.setAttribute("marginwidth", "1");
		ifrm.setAttribute("seamless", "seamless");
		ifrm.setAttribute("scrolling", "no");
		ifrm.setAttribute("allowtransparency", "true");
		ifrm.frameBorder = 0;
		ifrm.style.position = "fixed";
		ifrm.style.backgroundColor = "transparent";
		ifrm.style.height = "0px";
		ifrm.style.bottom = "0";
		ifrm.style.left = "0";
		ifrm.style.zIndex = "100";
		document.body.appendChild(ifrm);
	}

	function loginFromExtension(userName, password) {
		var theObject = {
			method : 'loginFromExtension',
			userName : userName,
			password : password,
		};
		var message = JSON.stringify(theObject);
		var origin1 = document.location.origin;
		window.parent.postMessage(message, origin1);
	}
	function forceLoginFromExtension(userName, password) {
		var theObject = {
			method : 'forceLoginFromExtension',
			userName : userName,
			password : password,
		};
		var message = JSON.stringify(theObject);
		var origin1 = document.location.origin;
		window.parent.postMessage(message, origin1);
	}
	function logOutFromExtension() {
		var theObject = {
			method : 'logOutFromExtension'
		};
		var message = JSON.stringify(theObject);
		var origin1 = document.location.origin;
		window.parent.postMessage(message, origin1);
	}

	function doPostMessage(message, callback) {

		if (callback) {
			var theObject = JSON.parse(message);
			theObject.requestMethodId = registerCallback(theObject.method,
					callback);
			message = JSON.stringify(theObject);
		}
		var theIframe = document.getElementById("ameyoIframe");
		var origin = ameyoBaseUrl;
		theIframe.contentWindow.postMessage(message, origin);
	}

	function registerCallback(method, callback) {
		method += '_' + methodId;
		callbacks[method] = [ callback ];
		methodId++;
		return method;
	}
	function customShowCrm(phone, additionalParams, requestId) {
		var crmPage = document.getElementById('crm');
		var html = "<br> Sending request to get  CRM data for phone: " + phone
				+ " Additional Parameters" + additionalParams
				+ "<br>  Recieving Response.."
				+ "<br> Populating CRM data on the basis of" + "response.."
				+ "<br>Done";
		crmPage.innerHTML = crmPage.innerHTML + "<br>" + html;
	}

	function postMessageToChatIframe(theObject, origin) {
		try {
			var theIframe = document.getElementById("userChatIframe");
			theIframe.contentWindow.postMessage(theObject, origin);
		} catch (e) {
		}
	}

	function resizeChatIFrameToFitContent(noOfChatsActive) {
		try {
			var theIframe = document.getElementById("userChatIframe");
			if (noOfChatsActive == 0) {
				theIframe.style.width = "0px";
				theIframe.style.height = "0px";
				return;
			}
			var width = noOfChatsActive * 270;
			theIframe.style.width = width + "px";
			theIframe.style.height = "345px";
		} catch (e) {
		}
	}
	return {

		initialize : function() {
			window.onload = function() {
				var head = document.getElementsByTagName('head')[0];
				var link = document.createElement('link');
				link.rel = 'stylesheet';
				link.type = 'text/css';
				link.href = ameyoBaseUrl + '/ameyochatjs/ameyochat.css';
				link.media = 'all';
				head.appendChild(link);
				if (chatIntegrationEnabled) {
					addIframeForChatWindow();
				}
			};

			if (window.attachEvent) {
				window.attachEvent('onmessage', processPostMessage);
			} else {
				window.addEventListener('message', processPostMessage, false);
			}

		},

		api : {
			setRecordInfoForShowCrm : function(showCrmrequestId, recordId,
					recordName, recordType, additionalInfo, callback) {

				doPostMessage(JSON.stringify({
					method : RECORD_INFO_FOR_SHOW_CRM,
					showCrmrequestId : showCrmrequestId,
					recordId : recordId,
					recordName : recordName,
					recordType : recordType,
					additionalInfo : additionalInfo
				}), callback);
			}

		},

		disposeNDial : function(dispositionCode, phone) {
			if (typeof dispositionCode == 'undefined') {
				alert('no disposition code to dispose call');
				return;
			}

			if (typeof dispositionCode == 'Callback') {
				alert("disposition code cannot be Callback");
				return;
			}

			if (typeof phone == 'undefined') {
				alert('no number to dial');
				return;
			}
			var theObject = {
				method : 'disposeNDial',
				dispositionCode : dispositionCode,
				phone : phone,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		getDispositionCodes : function(campaignId) {
			var theObject = {
				method : 'getDispositionCodes',
				campaignId : campaignId,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		disposeCallByDisposition : function(dispositionCode) {
			if (typeof dispositionCode == 'undefined') {
				alert('no disposition code to dispose call');
				return;
			}

			var theObject = {
				method : 'disposeCallByDisposition',
				dispositionCode : dispositionCode,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		doLogin : function(username, password, authPolicy) {
			var theObject = {
				method : 'doLogin',
				username : username,
				password : password,
				authPolicy : authPolicy
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		doForceLogin : function() {
			var theObject = {
				method : 'doForceLogin',
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		doLogout : function() {
			var theObject = {
				method : 'doLogout'
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		populateNumberInDialBox : function(phone) {
			var theObject = {
				method : 'populateNumberInDialBox',
				phone : phone,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		selectExtension : function(callContextId, phone) {

			var theObject = {
				method : 'selectExtension',
				callContextId : callContextId,
				phone : phone
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},
		doDial : function(phone, customerId, additionalParams, searchable,
				customerRecords) {
			var theObject = {
				method : 'doDial',
				phone : phone,
				additionalParams : additionalParams,
				searchable : searchable,
				customerRecords : customerRecords
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		setLoginInfo : function(loginCredential) {

			var theObject = {
				method : 'setLoginCredentials',
				userId : loginCredential.userName,
				password : loginCredential.password
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);

		},

		setExtensionInfo : function(extensionInfo) {

			var theObject = {
				method : 'setExtensionMetadata',
				extensionName : extensionInfo.name,
				extensionPhone : extensionInfo.phone
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);

		},
		hideUI : function(uiElements) {
			var theObject = {
				method : 'configureUI',
				uiElements : uiElements
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		disposeChat : function(obj) {
			var theObject = {
				method : 'disposeChatById',
				data : obj,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},

		refreshTelephony : function(crtobjectId) {
			var theObject = {
				method : 'refreshTelephony',
				data : crtobjectId,
			};
			var message = JSON.stringify(theObject);
			doPostMessage(message);
		},
		registerCustomFunction : function(key, value) {
			customFunctions[key] = value;
		}

	}
})();
ameyo.integration.initialize();