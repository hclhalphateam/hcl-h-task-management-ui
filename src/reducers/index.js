import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import taskReducer from "./taskReducer";
import backlogReducer from "./backlogReducer";
import securityReducer from "./securityReducer";

export default combineReducers({
  errors: errorReducer,
  project: taskReducer,
  backlog: backlogReducer,
  security: securityReducer
});
