import { GET_TASKS, GET_TASK, DELETE_TASK } from "../actions/types";

const initialState = {
  projects: [],
  project: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TASKS:
      return {
        ...state,
        projects: action.payload
      };

    case GET_TASK:
      return {
        ...state,
        project: action.payload
      };

    case DELETE_TASK:
      return {
        ...state,
        projects: state.projects.filter(
          project => project.taskId !== action.payload
        )
      };
    default:
      return state;
  }
}
