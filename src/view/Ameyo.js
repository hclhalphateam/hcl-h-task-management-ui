import React, { Component } from 'react';
import { connect } from "react-redux";
import "../css/style.css"
import "../App.css"

class Ameyo extends Component {

    constructor(){
        super();
        this.crmOrigin='';
        this.ameyoBaseUrl = "http://172.23.2.133:8888/";
        // this.doCall= (number)=>      window.ameyo.integration.doDial(number); 
    }
    
	call(number){
        // this.doCall(number);
        console.log("do call methid is called usr")
        try{
            window.ameyo.integration.doDial(number);
        }
        catch(error){
            console.log("ameyo dial error");
            console.log(error);
        }
    }

    intialize(){
        console.log(window.ameyo);
        this.crmOrigin = window.location.href;
        let iframeDiv = document.getElementById("ameyoIframeDiv");
        let iframeUrl = this.ameyoBaseUrl + "ameyowebaccess/toolbarLogin-vl.htm?";
         iframeUrl = iframeUrl + "origin=" + this.crmOrigin;
        let iframeHtml = '<iframe className="col-md-12" frameBorder="0" height="535px" id="ameyoIframe" src="' + iframeUrl + '"></iframe>';
        iframeDiv.innerHTML = iframeHtml;

    }
    componentDidMount(){
        this.intialize()

        var setRecordForShowCrmCallback = function(response) {
            if (response.result) {
              console.log("%c crm callback response: ", "font-size:48px; color:pink")
              console.log(response)
              // var crmPage = document.getElementById('crmPage');
              // var html = "<p>" + "Response : SetRecordShowCrm ->"
              //     + response.result.status + "</p>";
              // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
            }
          };
          let vm = this;
          function customShowCrm(phone, additionalParams, requestId) {
            
            console.log("%c Ameyo event listener message: ", "font-size:48px; color:blue");
            console.log(phone );
            console.log(additionalParams);			
            console.log(requestId);	
            console.log(additionalParams.callType!==undefined);
            console.log(typeof additionalParams);
      
            let jsonAdditionalPrarams= JSON.parse(additionalParams);
            console.log(jsonAdditionalPrarams.callType);
            if(jsonAdditionalPrarams.callType!==undefined && jsonAdditionalPrarams.callType==="inbound.call.dial"){
      
              vm.setState({phoneincoming:true});
              vm.setState({incomingphone:phone});
        
            } 
            console.log(vm.state.incomingphone);
            console.log(vm.state.phoneincoming);
            window.ameyo.integration.api.setRecordInfoForShowCrm(requestId, requestId,"","", phone,
              setRecordForShowCrmCallback);
          
          }
          
          let customIntegration = {};
          customIntegration.showCrm = customShowCrm;
          // customIntegration.showCrmDetailed = showCrmDetailedCustom;
          window.ameyo.integration.registerCustomFunction("showCrm", customIntegration);
      
          // function customCallDispose(userCustomerCRTInfo) {
              
          //   console.log("%c Ameyo event:: call dispose: ", "font-size:48px; color:orange");
          //   console.log(userCustomerCRTInfo);	
          //   // alert(userCustomerCRTInfo);
          // } 
      
          // customIntegration.handleDisposeCall = customCallDispose;
          // window.ameyo.integration.registerCustomFunction("handleDisposeCall", customIntegration);
      
         
         
          function handleHangup(reason, userCustomerCRTInfo) {
                    
            console.log("%c Ameyo event:: call dispose: ", "font-size:48px; color:orange");
            console.log(reason);	
            console.log(userCustomerCRTInfo);	
            vm.setState({phoneincoming:false});
            
            console.log(vm.state.phoneincoming);
          }
      
      
          customIntegration.hangupHandler = handleHangup;
          window.ameyo.integration.registerCustomFunction("hangupHandler", customIntegration);
      
      
          function showCrmDetailedCustom(recordInfo) {
            // var crmPage = document.getElementById('crmPage');
            // var html = "Record Id : " + recordInfo.recordId + "<br> User CRT info : "
            //         + recordInfo.userCustomerCRTInfo.userCrtObjectId
            //         + "<br> Customer CRT Info : "
            //         + recordInfo.userCustomerCRTInfo.customerCrtObjectId
            // crmPage.innerHTML = html + "<br>" + crmPage.innerHTML;
            console.log("%c showCrmDetailedCustom message: ", "font-size:48px; color:grey");
      
            console.log(recordInfo.recordId);
      
            // alert(recordInfo.recordId);
        }
      
          customIntegration.showCrmDetailed = showCrmDetailedCustom;
          window.ameyo.integration.registerCustomFunction("showCrmDetailed", customIntegration);
      

    }

    render() {
        return (
          <div className="row">
            <div id="ameyoIframeDiv"/>
            {/* <button onClick={
                ()=>this.call("8130578961")
                }>Call Vivek</button> */}
            </div>
        );
    }
}

export default connect(
  )(Ameyo);